package Java4_面向对象;

public class java03_属性 {
    public static void main(String[] args) {
        // todo 面向对象-属性
        //所谓的属性，其实就是类的对象的相同特征;
        // 结构;
        //  属性类型 属性名称=属性值;
        // 如果在声明属性的同时进行了属性的初始化，那么所有的对象的属性都相同;
        // 所以如果希望每一个属性的值都不一样，那么可以不用在声明属性的时候进行初始化;
        // 那么属性会在创建的时候默认初始化,而默认初始化的值取决于属性的类型;
        /*
          属性的默认值:
            1.整数型 byte,short,int,long->0;
            2.浮点型 float.double->0.0;
            3.布尔类型: boolean->false;
            4.字符: char->空字符;
            5.引用数据类型: ->null;
         */
        // 变量和属性的区别
        // 变量的作用域很小，只能在当前的大括号中使用;
        // 属性不仅仅是在当前类中有效，而且可以随着对象在不同类中使用;
        // 变量在使用前必须初始化，否则会出现错误，属性可以不用初始化，因为jvm会帮助我们自动完成初始化;
        User05 user05 = new User05();
        user05.toString();
    }
}

class User05 {
    //String name ="张三";
    String name;
    byte a;
    float f;
    boolean b;
    char c;

    @Override
    public String toString() {
        return "User05{" +
                "name='" + name + '\'' +
                ", a=" + a +
                ", f=" + f +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
