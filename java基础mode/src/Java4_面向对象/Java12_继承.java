package Java4_面向对象;

public class Java12_继承 {
    public static void main(String[] args) {
        // todo 面向对象-继承
        // todo 关键字 extends super this
        // super & this 是用来区分父类和子类的属性和方法
        // 类的继承只能是单继承
        // 类存在父子关系:子类可以继承父类的成员属性和成员方法;
        //一个父类可以有多个子类;
        // todo 构造方法
        // 父类对象是在子类对象创建前创建完成的,会调用父类的构造方法完成父类的创建;
        Child child = new Child();
        Child child2 = new Child();
        Child child3 = new Child();
        child.test();
    }
}

/**
 * 父类
 */
class Prent {
    String name="张三";
    Prent(){
        System.out.println("父类");
    }
    Prent(String name){
        name=name;
        System.out.println("父类");
    }

    void test(){
        System.out.println("test");
    }

}
/**
 * 子类
 */
class Child extends Prent{

    Child(){
        // super("王二") 调用父类的构造方法
        super("王二");
        System.out.println("子类");
    }

   String name="李四";
    void test(){

        System.out.println(this.name);
        System.out.println(super.name);
    }
}