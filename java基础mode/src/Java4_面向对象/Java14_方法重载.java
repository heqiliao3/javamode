package Java4_面向对象;

public class Java14_方法重载 {
    public static void main(String[] args) {
        // todo 面向对象-方法重载
        // 一个类中，不能重复声明的相同的方法,也不能声明相同的属性
        //  TODO 这里相同的方法指的是方法名，参数列表相同，和返回值无关
        // TODO 如果方法名相同，但是参数列表（个数，顺序，类型）不相同，会认为是不同的方法，只不过名称一样
        // 这个操作在java称之为方法的重载
        // 引用数据类型 BB->AA-Object
        // 基本数据类型 type->short->int->long   type不能转换为char
        // todo 重要： 方法的重载如果找不到当前类型，它会向上转型去找;引用数据类型会找它的父类;
        Java14 java14 = new Java14();
        java14.login("wx");
        java14.login(123456);
        java14.login("张三","123456");
    }
}

class Java14{
    void login(String username, String password){
        System.out.println("账号，密码登录");
    }
    void login(int letter){
        System.out.println("验证码登录");
    }
    void login(String wx){
        System.out.println("微信，支付宝登录");
    }
}
