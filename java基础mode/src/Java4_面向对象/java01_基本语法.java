package Java4_面向对象;

public class java01_基本语法 {


    public static void main(String[] args) {
        // todo 面向对象
        // 类和对象
        // 类表示归纳和整理
        // 对象就表示具体的事务；
        // todo class（类）: class 用于声明类
    /*
      基本语法语法:
      class 类名{
        特征（属性）
        功能（方法）
      }
     */
     /*
       创建对象的语法
         new 类名();
      */
        // todo 1.先声明类
        // todo 2.创建对象
        // todo 3.声明属性，所谓的属性其实就是类中的变量
        // todo 4.声明方法;
      /*
         void 方法名（参数）{
          功能代码;
         };
       */
        //new Cooking() 属于new了一个新的对象，
        //Cooking cookie=new Cooking() 创建一个变量cookie去引用new Cooking()的对象，然后获取这个对象的属性和方法，属于引用数据类型：
        Cooking cookie=new Cooking();
        cookie.name="红烧排骨";
        cookie.food="排骨";
        cookie.execute();

    }

}

/**
 * 菜类
 */
class Cooking{
    /**
      菜的名字
     */
    String name;
    /**
     * 菜的食材
     */
    String food;
    /**
     * 菜的类型
     */
    String type="红烧";
    /**
     * 菜的佐料
     */
    String relish="十三香";

    // todo 执行
    void execute(){
        System.out.println("准备食材:"+food);
        System.out.println("准备佐料:"+relish);
        System.out.println("开始烹饪:"+type);
        System.out.println("烹饪完成:"+name);
    }
}