package Java4_面向对象;

public class Java17_访问权限 {
    public static void main(String[] args) {
        // todo 面向对象 -访问权限
        // public :公共的-访问权限修饰符
        // todo java中的访问权限主要分为4种：
        // 1.private :私有的,同一个类中可以使用
        // 2.(default):默认权限，当不设置任何权限时，jvm会默认提供权限，包(路径)权限
        // 3.protected: 受保护的权限，子类可以访问
        // 4.public :公共的，任意的;

    }
}
