package Java4_面向对象;

public class Java25_bean规范 {
    public static void main(String[] args) {
        //todo 面向对象-bean规范
        //1.主要用于编写逻辑
        //2.主要用于建立数据模型(bean)
        //我们把User2的对象称之为bean对象,的变量称之为bean变量
        User2 user2 = new User2();
        user2.setAccount("admin");
        user2.setPassword("admin");
        login(user2);
    }

    public static boolean login(User2 user2){
        if(user2.getAccount().equals("admin") && user2.getPassword().equals("admin")){
            return true;
        }else {
            return false;
        }
    }
}

class User2 {

    private String account;
    private String password;

    public String getAccount() {
        return account;
    }

    public String getPassword() {
        return password;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
