package Java4_面向对象;

public class Java04_方法 {
    public static void main(String[] args) {
        // todo 面向对象->方法
        //语法: void 方法名（）{逻辑代码}
        // void 就是方法的结果;
        // 调用方式:对象，方法名();
        Java04 java04 = new Java04();
        boolean register = java04.register();
        if (register){
            System.out.println("注册成功");
            boolean login = java04.login();
            if (login){
                System.out.println("登录成功");
            }else {
                System.out.println("登录失败");
            }
        }else {
            System.out.println("注册失败");
        }


    }
}

/**
 * 使用用户账号和密码进行登录
 * 名词: 用户，账号，密码
 * 动词: 登录
 */
class Java04{

    String account;
    String password;

    boolean register(){
        System.out.println("用户注册");
        return true;
    }

    boolean login(){
        System.out.println("用户登录");
        return false;
    }

}
