package Java4_面向对象;

public class Java21_抽象 {

    public static void main(String[] args) {
        // todo 面向对象-抽象
        // 抽象类:不完整的类，就是抽象类，因为不完整，所以无法直接构造对象；
        // 抽象方法：只有声明 ，没有实现的方法 abstract 返回参数 方法名(参数);
        // 如果一个类中含有抽象方法，那么这个类是抽象类;
        // 如果一个类是抽象类，它的方法不一定是抽象方法;
        // 抽象类无法直接构建对象，可以通过子类间接的构建对象
        // 如果抽象类中含有抽象方法，那么子类继承抽象类，需求重写抽象方法，将方法补充完整；
        // abstract关键字和final关键字不能同时使用：
        Java21 java211 = new Java211();
        java211.EAttribute();
    }
}

abstract class  Java21{
    public abstract void  EAttribute();
}

 class  Java211 extends Java21{

     @Override
     public void EAttribute() {
         System.out.println("中国人用筷子吃饭");

     }
 }
