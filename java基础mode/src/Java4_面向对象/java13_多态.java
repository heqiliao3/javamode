package Java4_面向对象;

public class java13_多态 {

    public static void main(String[] args) {
        // todo 面向对象 多态
        // 所谓的多态，其实就是一个对象在不同场景下表现出来的不同状态和形态;
        // 多态语法其实就是对对象的使用场景就行了约束
        // 一个对象可以使用的功能取决于引用变量的类型
        Prent2 prent1 = new Boy();
        prent1.test();
        Prent2 prent2 = new Girl();
        prent2.test();
        Prent2 prent3 = new Prent2();
        prent3.test();
        Boy boy = new Boy();
        boy.testBoy();
        boy.test();
        Girl girl = new Girl();
        girl.testGirl();
        girl.test();
    }
}

/**
 * 父类
 */
class Prent2 {
    void test(){
        System.out.println("父类");
    }

}
class Boy extends Prent2{
    void testBoy(){
        System.out.println("男孩");
    }

}
class Girl extends Prent2 {
    void testGirl(){
        System.out.println("女孩");
    }

}