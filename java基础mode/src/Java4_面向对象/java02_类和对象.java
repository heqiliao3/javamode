package Java4_面向对象;

public class java02_类和对象 {
    public static void main(String[] args) {
        // todo 类和对象
        // 类:结构体，里面包含了属性（特征）和方法（行为）;
        //    会有很多对象;
        // class :关键字都是小写的
        // 类名: 类的名称首字母大写
        // 对象: 类的实例化;
        // user04变量的类型就是对象的类型;
        // todo 类 对象 变量 的关系
        // 对象:存在堆内存中的；
        new User04();

        // 变量和方法:存在栈内存中;
        //user041和show()都在栈中
        User04 user041 = new User04();
        user041.show();
        // 类: 存在元空间内存中的;
        //User04类;
        //这个张三在常量池中;
        String name="张三";
        // todo 对象是将内存地址赋值给了变量，所以变量其实引用了内存中的对象，所以称之为引用变量；
        // todo 而变量的类型称之为引用数据类型;

        User04 user04 = new User04();
        // todo 特殊的对象：空对象。没有引用的对象,称之为空对象，关键字对象；(就是说声明了变量在栈空间中，堆空间是没有对象的，所以没有引用)
        // 所有引用类型变量的默认取值就是null;
        User04 user05 =null;
    }
}

class User04{

    void show(){}

}
