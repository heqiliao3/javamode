package Java4_面向对象;

public class Java05_方法参数 {
    public static void main(String[] args) {
        // todo 面向对象-> 方法参数
        // 使用外部数据控制方法内部的实现的操作,使用的是参数语法的实现, 也叫方法参数;
        // 语法: 方法名（方法类型 方法名称）
        Java05 java05 = new Java05();
        String name="张三";
        java05.sayHello(name);
        int age=30;
        java05.sayHello(name,age);
       // todo 可变参数  参数类型... 参数名称
        java05.sayHello2();
        java05.sayHello2(name,name);
        java05.sayHello2(name,name,name);
        java05.sayHello3(30,name,name,name);

    }
}
class Java05{

    void sayHello(String name){
        System.out.println("hello"+name);
    }

    void sayHello(String name,int age){
        System.out.println("hello"+name);
    }

    /**
     * 可变参数类型
     * @param name
     */
    void sayHello2(Object... name){
        System.out.println("hello"+name);
    }
    void sayHello3(int age,Object... name){
        System.out.println("hello"+age+name);
    }


}