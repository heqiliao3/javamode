package Java4_面向对象;

import java.util.ArrayList;
import java.util.Date;

public class java10_import {
    public static void main(String[] args) {
        // todo 面向对象-import
        // import 关键字导入包的路径
        // 如果一个包需要导入大量的类，那么可以使用通配符*来简化操作;
        String name="张三";
        Date date = new Date();
        ArrayList arrayList = new ArrayList();
    }
}
