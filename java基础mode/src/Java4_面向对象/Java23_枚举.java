package Java4_面向对象;

public class Java23_枚举 {
    public static void main(String[] args) {
        // todo 面向对象-枚举
        // 枚举是一个特殊的类，其中包含了一个组特殊的对象，这些对象不会发生改变，一般都使用大写的标识符
        // 枚举使用enum关键字使用
        // 枚举类会将对象放置在最前面，那么和后面的语法需要使用分号隔开;
        // 枚举类不能创建对象，它的对象是在内部类中自行创建
        System.out.println(City.SHANGHAI.name);
        System.out.println(City.BEIJING.code);

    }
}

enum City{
    BEIJING("北京",001),SHANGHAI("上海",002);

    public String name;
    public Integer code;
    City(String name,Integer code){
        this.name=name;
        this.code=code;
    }

}
