package Java4_面向对象;

public class Java20_final {
    public static void main(String[] args) {
        // todo 面向对象-final
        //java中提供了一种语法，可以在数据库初始化后不被修改，使用关键字final;
        // final可以修饰变量，变量的值一旦初始化后无法修改；
        //一般被final修饰的变量称之为常量，或者不可变变量
        //final修饰的方法不能被重写；
        //final修改的类不能被继承
        //final修饰的变量不能被修改，变量必须初始化；
        //final不能修饰构造方法；
        final String name="张三";
        //name = "李四";
    }

}

class Java20{
    //final可以修饰属性：那么jvm无法自动进行初始化，需要自己进行初始化。属性值不能发生改变；
    //public final String name;
    public final String name;
    Java20(String name){
        this.name=name;
    }

    public final void text(){

    }
}

