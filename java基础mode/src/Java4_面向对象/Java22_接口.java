package Java4_面向对象;

public class Java22_接口 {
    public static void main(String[] args) {
        // todo 面向对象-接口
        // 所谓的接口：可以简单理解为规则
        // 基本语法: interface 接口名称{规则属性，规则的行为};
        //接口其实就是抽象的
        // 规则的属性必须为固定值，而且不能修改；
        // 属性和行为的访问权限必须为公共的
        // 属性应该为静态的
        // 行为应该是抽象的
        // 接口和类是两个层面的东西
        // 接口可以继承其他接口
        // 类可以实现多个接口：
        电脑 d = new 电脑();
        台灯 t = new 台灯();
        d.usb1=t;
        d.提供能源();
        台灯 t2 = new 台灯();
        d.usb2=t2;
        d.提供能源();


    }
}
interface Usb{

}
interface Java22_1 extends Usb{

    public void 提供能源();

}
interface Java22_2 extends Usb{

    public void 消耗能源();

}

class 电脑 implements Java22_1{
    Java22_2 usb1;
    Java22_2 usb2;


    @Override
    public void 提供能源() {
        System.out.println("电脑提供能源");
        usb1.消耗能源();
        usb2.消耗能源();
    }
}
class 台灯 implements Java22_2{

    @Override
    public void 消耗能源() {
        System.out.println("消耗能源");
    }
}