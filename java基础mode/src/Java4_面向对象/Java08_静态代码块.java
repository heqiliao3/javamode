package Java4_面向对象;

public class Java08_静态代码块 {
    public static void main(String[] args) {
        // todo 面向对象-静态代码块
        //类的信息加载完成后，会自动调用静态代码块,可以完成静态属性的初始化功能;
        //代码块是类创建对象的时候执行
        Java08.java08();
        //Java08 java08 = new Java08();
    }
}

class Java08{
    //静态代码块，创建类的时候执行：
    static {
        System.out.println("静态代码块");
    }
    //代码块，创建对象的时候执行
    {
        System.out.println("代码块");
    }

    static void java08(){
        System.out.println("java08");
    }
}
