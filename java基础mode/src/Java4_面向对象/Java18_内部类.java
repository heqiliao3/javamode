package Java4_面向对象;

public class Java18_内部类 {
    public static void main(String[] args) {
        // todo 面向对象-内部类
        // java不允许外部类使用private修改
        // 所谓的外部类，就是在源码中直接声明的类
        // 所谓的内部类，就是类中声明的类；
        // 内部类就当成外部类的属性使用即可;
        //因为内部类可以看作外部类的属性，所以需要构建外部类对象才可以使用;
        Java18 java18 = new Java18();
        Java18.InsertClass insertClass = java18.new InsertClass();
        insertClass.insertSelective();
    }


}

class Java18{
    /**
     * 内部类
     */
    public class InsertClass{
       public void insertSelective(){
           System.out.println("hello");
       }
    }
}

