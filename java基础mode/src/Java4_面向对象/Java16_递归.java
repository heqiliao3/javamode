package Java4_面向对象;

public class Java16_递归 {
    public static void main(String[] args) {
        // todo 递归
        // 所谓的递归：方法调用自身,称之为递归方法;
        //递归方法应该有跳出的逻辑；
        //调用自身时,传递的参数需要有规律；

        //1+3+5+7+9 10以内的奇数运算；
        int i = computeAP(10);
        System.out.println("10以内奇数");
        System.out.println(i);
        //阶乘 5！-> (4.3.2.1)->5*4*3*2*1
        int j = computeAP2(5);
        System.out.println("阶乘");
        System.out.println(j);

    }

    public static int computeAP(int num){
        num = num % 2 == 0 ? num - 1 : num;
        if(num==1){
            return 1;

        }else {
            return num+computeAP(num-2);
        }

    }
    public static int computeAP2(int num){
        if(num<=1){
            return 1;

        }else {
            return num * computeAP2(num-1);
        }

    }

}
