package Java4_面向对象;

public class java06_值传递 {

    public static void main(String[] args) {
        //todo 面向对象-参数传递（值传递）
        //java方法参数的传递为值传递
        // 基本数据类型:数值
        // 引用数据类型:引用地址

        //1,基本数据类型由于变量和值都存在栈中，有压栈和弹栈导致,修改后的数据弹栈出去了，所以还是原来的数据;
        int a =10;
        test(a);
        System.out.println(a);
        //2.String因为被final修饰，所以它的对象是不可修改的，如果修改会创建一个新的对象在堆内存中;
        String name ="张三";
        test(name);
        System.out.println(name);
        //3.StringBuffer的对象是可以修改的，所以他可以修改堆内存中的对象;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("张三");
        test(stringBuffer);
        System.out.println(stringBuffer);
        //4
        User user = new User();
        user.name="张三";
        test(user);
        System.out.println(user.name);

    }

    public static  void test(int a) {
        int i = a + 10;
    }
    public static  void test(String name) {
        name  = name+10;
    }
    public static  void test(StringBuffer stringBuffer) {
        stringBuffer  = stringBuffer.append("李四");
    }
    public static  void test(User user) {
        user.name="李四";
    }
}
class User {
    String name;
}
