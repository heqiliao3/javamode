package Java4_面向对象;

public class Java26_作用域 {
    public static void main(String[] args) {
        //todo 面向对象-作用域
        Java26 java26 = new Java26();
        java26.test();

    }
}
class User08{
    public String name="王二";

}


class Java26 extends User08{

   public String name="张三";

   public void test(){
       String name="李四";
       //如果属性和(局部)变量的名称相同，访问时如果不加修饰符，那么优先访问变量；
       System.out.println(name);

   }
}
