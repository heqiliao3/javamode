package Java4_面向对象;

public class Java15_方法重写 {
    public static void main(String[] args) {
        // todo 面向对象-方法重写
        // 方法的重写： 父类对象的方法其实主要体现在通用性，无法在特殊的场合下使用
        //           如果子类对象需要在特殊的场合下使用，那么就需要重写方法的逻辑，这个操作在java中称只为方法的重写
        // todo 重要：方法重写的要求，子类的方法和父类的方法，方法名相同，返回类型相同，参数列表都要相同
        //一个方法的使用，是要看具体的对象的;
        //一个属性的使用，是要看具体的属性的;
        // 能用什么方法，取决于前面的变量，用什么方法取决于后面的对象
        // 能用什么属性，不需要看后面的对象;属性在哪里声明，就在哪里使用
        // 在当前类中this.name和name是一样的;
        Prent3 prent3 = new Boy2();
        prent3.test();
        Boy2 boy2 = new Boy2();
        boy2.test();
        System.out.println("--------------------------------");
        AAA aaa =new CCC();
        System.out.println(aaa.sum());
    }
}
/**
 * 父类
 */
class Prent3 {
    void test(){
        System.out.println("父类");
    }

}
class Boy2 extends Prent3{
    void test(){
        System.out.println("男孩");
    }

}
class Girl2 extends Prent3 {
    void test(){
        System.out.println("女孩");
    }

}
class  AAA {
    int i=10;
    int sum(){
        return geti()+10;
    }
    int geti(){
        return i;
    }

}

/**
 * 方法的重写，间接的使用子类的方法
 */
class CCC extends AAA {
    int i=20;
//    int sum(){
//        return geti()+10;
//    }
    int geti(){

        return i;
    }

}