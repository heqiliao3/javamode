package Java4_面向对象;

public class Java11_构造方法 {
    public static void main(String[] args) {
        // todo 面向对象-构造方法
        // 构造方法:是定义在java类中的一个用来初始化对象的方法，用new+构造方法，创建一个新的对象，并可以给对象中的实例进行赋值。
        // 功能：主要是完成对象数据的初始化;
        // 如果一个类中没有任何的构造方法，那么jvm会自动添加一个公共的，无参的构造方法，方法对象的调用；
        // todo 基本语法 : 类名(){}
        // 1.构造方法也是方法，但是没有void关键字
        // 2.方法名和类名完全 相同；
        // 3.如果类中没有构造方法，那么jvm会提供默认的构造方法
        // 4.如果类中有构造方法，那么jvm不会提供默认的构造方法；
        // 5.构造方法也是方法，所以也可以传递参数，但是一般传递参数的目的是用于对象属性的赋值;
        System.out.println("创建对象前");
        Java11 java11 = new Java11("中国");
        System.out.println("创建对象后");
        java11.show();
        System.out.println(java11.type);
        //代码块:是创建对象之前执行的;
    }

}
class Java11{
    {
        System.out.println("代码块1");
    }
    String type;
    Java11(String type2){
        type=type2;
    }
    void show(){
        System.out.println("show");
    }
}
