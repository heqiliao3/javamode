package Java4_面向对象;

public class java07_静态 {
    public static void main(String[] args) {
      // todo 面向对象-静态
        // 把和对象无关，之和类相关的称只为静态
        // 和类相关的属性称之为静态属性
        // 和类相关的方法称之为静态方法
        // 静态语法就是在属性和方法前增加static关键字
        System.out.println(Channel.type);
        Channel.show();
        //先有类，再有对象;
        //成员属性可以访问静态属性和静态方法的;
        //静态方法是不能访问成员属性和成员方法的;
        Test test = new Test();
        test.sex="女";
        test.test1();
    }
}
class Test{
    String name;
    static String sex;
    //成员方法
    void test1(){
        System.out.println("Test");
        test2();
        System.out.println(sex);
        System.out.println("test1");
    }
   //静态方法
   static  void test2(){
       System.out.println("test2");

    }
}
class Channel {

    static String type="鸟";
    public static void  show() {
        System.out.println("Channel");
        System.out.println("飞");
    }
}