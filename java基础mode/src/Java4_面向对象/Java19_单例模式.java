package Java4_面向对象;

public class Java19_单例模式 {
    public static void main(String[] args) {
        //todo 面向对象-单例模式
        //1.类的创建过程是复杂的
        //2.类的对象是消耗资源的
        // 单例模式就是说不管你能多少次，只创建一个对象，来解决对象消耗资源的问题
        // 但是单例模式在多线程的时候有问题;
        Java19 java19 = Java19.getJava19();


    }
}

class Java19{

    private static Java19 java19=null;

    private Java19(){

    }

    public static Java19 getJava19(){
        if(java19==null){
            java19 = new Java19();
        }
        return java19;
    }
}
