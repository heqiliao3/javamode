package Java4_面向对象;

public class Java24_匿名类 {
    public static void main(String[] args) {
        //todo 面向对象-匿名类
        // 在模型场合下，类的名字不重要，我们只有使用类的方法和功能，那么此时我们采用特殊的语法：匿名类
        // 所谓的匿名类就是没有名字的类
        //抽象类使用匿名类
        Dongwu d = new Dongwu() {
            public void pao() {
                System.out.println("跑");
            }
        };
        d.pao();
        //接口使用匿名类
        Niao f = new Niao() {
            public void fei() {
                System.out.println("飞");
            }
        };
        f.fei();
    }
}

/**
 * 抽象类
 */
abstract class  Dongwu {
    public abstract void pao();
}

/**
 * 接口
 */
interface Niao{
    public void fei();
}
