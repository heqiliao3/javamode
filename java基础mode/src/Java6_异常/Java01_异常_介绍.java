package Java6_异常;

public class Java01_异常_介绍 {
    public static void main(String[] args) {
        // todo 异常-介绍
        // TODO JAVA中异常分为2大类
        //      1.可以通过代码恢复的正常逻辑执行的异常，称只为运行异常：RuntimeException;
        //      2.不可以通过代码恢复正常逻辑执行的异常，称之为编译异常：Exception;
        //1.类型转换错误
        String s="123";
       // Integer i=(Integer)s;
        //2.递归没有跳出的逻辑:StackOverflowError 栈溢出错误;
        test();
        //3.空指针异常错误:NullPointerException
        User10 user10=null;
        System.out.println(user10.toString());
    }

    public static void test(){
        test();
    }
}

class User10{

}
