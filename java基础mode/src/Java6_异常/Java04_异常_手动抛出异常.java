package Java6_异常;

public class Java04_异常_手动抛出异常 {
    public static void main(String[] args) {
        // todo 异常-手动抛出异常
        //    常处理方式三:throw
       // Singleton2.register(-10);
        try {
            Singleton.register(-10);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}

/**
 * 手动抛运行时异常RuntimeException
 */
class Singleton2{
    private int id;
    public static void register(int id){
        if(id >0){
            System.out.println(id);
        }else {
            throw new RuntimeException("不是整数！");
        }
    }
}

/**
 * 手动抛编译时异常Exception
 */
class Singleton{
    private int id;
    public static void register(int id) throws Exception {
        if(id >0){
            System.out.println(id);
        }else {
            throw new Exception("不是整数！");
        }
    }
}