package Java6_异常;

public class Java05_异常_自定义异常 {
    public static void main(String[] args) {
        // todo 异常-自定义异常
        login("admin","bbb");
    }

    public static void login(String username, String password){
        if(!"admin".equals(username)){
               throw new AccountExtension("账号错误");
        }
        if(!"admin".equals(password)){
            try {
                throw new PasswordExtension("密码错误");
            } catch (PasswordExtension e) {
                throw new RuntimeException(e);
            }
        }


    }
}


class AccountExtension extends RuntimeException{

    public AccountExtension(String message){
         super(message);
    }
}

class PasswordExtension extends Exception{

    public PasswordExtension(String message){
        super(message);
    }
}