package Java6_异常;

public class Java03_异常_throws {
    public static void main(String[] args) {
      // todo 异常处理方式一 : throws +异常类型
      //      throws+异常类型 写在方法的声明处，指明此方法执行时，可能会抛出的异常
      //     一旦当方法体执行时，出现异常，仍会在异常代码处生成一个异常类的对象。此对象满足throws后异常类型时，就会被抛出，异常代码后续的代码，就不会在执行;
      //  体会:
      //    1.try_catch_finally:真正的将异常处理掉了
      //    2.throws:的方式只是将异常抛给了方法的调用者，并没有真正将异常处理掉

        int i = 0;
        int j = 10;

        try {
            test(j,i);
        }catch (ArithmeticException e) {
            e.printStackTrace();
        }
        System.out.println("哈哈哈哈");

    }

    public static int test(int x, int y) throws ArithmeticException  {
        return x / 0;
    }
}
