package Java6_异常;

public class Java02_异常_try_catch {
    public static void main(String[] args) {
        // todo 异常处理方式一 : try{ }catch(){}... finally{}
        //       捕获异常并处理掉
        // 使java程序更加健壮，没有那么容易挂掉
        /*

         异常处理语句:

          todo try:尝试
          todo catch:捕获
             捕获多个异常的时候，需要先捕获范围小的异常，然后再捕获范围大的异常；
          todo finally:最终

          try{
             可能出现异常的代码
             如果出现异常，那么jvm会将异常进行封装，形成一个具体的异常类，然后将这个异常捕获
          }catch(抛出的异常对象 对象引用){
              异常的解决方案
          }catch(){

          }finally{
             最终的执行代码
          }
         */
        int i=0;
        int j=0;
        try {

            j = 10 / i;

        }catch(ArithmeticException e){
            //e.getMessage();//错误的消息
            //e.getCause(); //错误的原因
            e.printStackTrace(); //一般用的比较多的， 打印，可以打印堆栈错误信息
        }finally{
            System.out.println("最终执行的代码");
        }
        System.out.println(j);

    }
}
