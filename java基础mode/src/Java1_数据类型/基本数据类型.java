package Java1_数据类型;

/**
 * 数据类型
 *
 * @author maqing
 * @date 2022/12/09
 */

public class 基本数据类型 {
    public static void main(String[] args) {
        String name ="zhangsan";
        // todo 1.数据得存储单位
        //1.比特（bit位）: 数据运算的最小存储单位；
        //2.字节(byte):数据最小的存储单位;
        //字节和比特是可以相互转换的
        // 1byte=8bit;
        // 平时的存储单位还有:KB,MB GB TB ...PB,EB;
        //1024 byte=>1kb;
        //1024 kb=1 mb;

        // TODO: 基本数据类型

        // TODO: 1.整数类型
        // byte:8位；
        byte b=8;
        // short :16位；
        short s=16;
        //int : 32位
        int x=32;
        //long : 64位;
        long l=64;
        // TODO: 2.浮点类型:含有小数点的数据类型；
        // 根据计算精度分为
        // float : 单精度浮点类型(数据需要使用F(f)结尾);
        float f=20.1f;
        // double : 双精度浮点类型;
        double e=20.11;
        // TODO 3.字符类型
        // 所谓的字符类型，其实就是使用符号标识的文字内容
        // char 只能存储一个字符;
        char c = '中';
        // TODO 4.布尔类型
        // true false 判断条件是否成立，成立返回true,不成立返回false;
        boolean bln = true;

        // TODO 数据类型的转换
        // java 默认数据范围小的数据类型可以自动转换为数据类型大的数据类型，但是大的数据类型不能转成小的数据类型
        // byte-->short-->int-->long-->float-->double
        byte b1=1;

        short sho=b1;

        int in =sho;

        long ong=in;

        float fl=ong;

        double dou=fl;

        //如果想要将范围大的数据类型转换为范围小的数据类型，我们可以通过强制转换;
        double doubl=20.22;
        float flt=(float)doubl;
    }
}
