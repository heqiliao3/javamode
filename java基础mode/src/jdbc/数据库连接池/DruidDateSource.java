package jdbc.数据库连接池;

import jdbc.entity.Customers;
import java.util.List;

/**
 * druid数据库连接池测试
 */
public class DruidDateSource {


    public static void main(String[] args) throws Exception {
        String sql="select * from customers where id=?";
        List<Customers> list = DruidUtils.generalQuery(Customers.class, sql, 1);
        list.forEach(System.out::println);
    }





}
