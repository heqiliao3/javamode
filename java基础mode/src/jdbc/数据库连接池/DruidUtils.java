package jdbc.数据库连接池;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import jdbc.config.JDBCUtils;

import javax.sql.DataSource;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;

/**
 * druid数据库连接池
 */
public class DruidUtils {

    public static Connection getConnertion(){
        try {
            Properties properties = new Properties();
            InputStream resourceAsStream = DruidUtils.class.getClassLoader().getResourceAsStream("druid.properties");
            properties.load(resourceAsStream);
            DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);
            Connection connection = dataSource.getConnection();
            System.out.println(connection);

            return connection;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 通用的查询操作 Class<T> clazz （任意）实体，查询多条数据
     */
    public static  <T> List<T> generalQuery(Class<T> clazz, String sql, Object... args) throws Exception {
        PreparedStatement preparedStatement=null;
        ResultSet rs=null;
        Connection connertion=null;
        try {
            //1.获取数据库连接
            connertion = getConnertion();
            //2.预编译sql
            preparedStatement = connertion.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            // 4.执行executeQuery(),得到结果集：ResultSet  ResultSet没有办法获取结果的数量，只有用ResultSetMetaData才能获取到;
            rs=preparedStatement.executeQuery();
            //5.得到结果集的元数据： ResultSetMetaData,通过元数据获取结果的数量
            ResultSetMetaData rsmd = rs.getMetaData();
            //6.通过ResultSetMetaData得到columnCount(数量);
            int columnCount = rsmd.getColumnCount();
            ArrayList<T> list = new ArrayList<>();
            while (rs.next()){
                T t = clazz.newInstance();
                //处理结果集一行数据中的每一列,给T对象指定的属性赋值;
                for (int i = 0; i <columnCount; i++) {
                    //获取列值
                    Object columnVal = rs.getObject(i + 1);
                    //获取列明
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //7.2使用反射，给对象的相应属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t,columnVal);
                }
                list.add(t);


            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            JDBCUtils.closeResource(connertion,preparedStatement,rs);
        }
        return null;
    }
}
