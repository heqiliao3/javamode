package jdbc.PreparedStatement;

import jdbc.config.JDBCUtils;
import jdbc.entity.User;
import org.junit.Test;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/**
 * 通用方法查询一条，多条，增删改
 */
public class PreparedStatementQueryTest {

    /**
     * 通用的查询操作 Class<T> clazz （任意）实体，查询一条数据
     */
    public <T> T Select2(Class<T> clazz, String sql, Object... args) throws Exception {
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet rs=null;
        try {
            //1.获取数据库连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            // 4.执行executeQuery(),得到结果集：ResultSet  ResultSet没有办法获取结果的数量，只有用ResultSetMetaData才能获取到;
            rs=preparedStatement.executeQuery();
            //5.得到结果集的元数据： ResultSetMetaData,通过元数据获取结果的数量
            ResultSetMetaData rsmd = rs.getMetaData();
            //6.通过ResultSetMetaData得到columnCount(数量);
            int columnCount = rsmd.getColumnCount();
            if(rs.next()){
                T t = clazz.newInstance();
                for (int i = 0; i <columnCount; i++) { //遍历每一行
                    //获取列值
                    Object columnVal = rs.getObject(i + 1);
                    //获取列明
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //7.2使用反射，给对象的相应属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t,columnVal);
                }
                System.out.println("执行成功:t"+t);
                return t;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            JDBCUtils.closeResource(connection,preparedStatement,rs);
        }
        return null;
    }



    /**
     * 通用查询测试
     */
    @Test
    public void testSlerct() throws Exception {

        String sql="select * from user where name=? and id=?";
        Select2(User.class,sql,"张四",7);
    }


    /**
     * 通用的查询操作 Class<T> clazz （任意）实体，查询多条数据
     */
    public <T> List<T> Select3(Class<T> clazz, String sql, Object... args) throws Exception {
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet rs=null;
        try {
            //1.获取数据库连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            // 4.执行executeQuery(),得到结果集：ResultSet  ResultSet没有办法获取结果的数量，只有用ResultSetMetaData才能获取到;
            rs=preparedStatement.executeQuery();
            //5.得到结果集的元数据： ResultSetMetaData,通过元数据获取结果的数量
            ResultSetMetaData rsmd = rs.getMetaData();
            //6.通过ResultSetMetaData得到columnCount(数量);
            int columnCount = rsmd.getColumnCount();
            ArrayList<T> list = new ArrayList<>();
            while (rs.next()){
                T t = clazz.newInstance();
                //处理结果集一行数据中的每一列,给T对象指定的属性赋值;
                for (int i = 0; i <columnCount; i++) {
                    //获取列值
                    Object columnVal = rs.getObject(i + 1);
                    //获取列明
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //7.2使用反射，给对象的相应属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t,columnVal);
                }
                list.add(t);


            }
          return list;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            JDBCUtils.closeResource(connection,preparedStatement,rs);
        }
        return null;
    }

    @Test
    public void Select3Test() throws Exception {
        String sql="select * from user where  id>?";
        List<User> users = Select3(User.class, sql,  7);
        users.forEach(System.out::println);
    }


    /**
     * 3.//通用的增、删、改操作（体现一：增、删、改 ； 体现二：针对于不同的表）
     */
    public void updatedDL(String sql,Object... args){
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        try {
            //1.获取数据库的连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i <args.length ; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            //4.执行sql
            /**
             *  方式一：execute返回boolean:
             *         如果第一个结果是ResultSet对象，则为true；如果第一个结果是更新计数或没有结果，则为false
             */
            //preparedStatement.execute();
            /**
             * 方式二：executeUpdate返回int
             *       （1）SQL数据操作语言（DML）语句的行数，或（2）不返回任何内容的SQL语句的行数为0
             */
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResource(connection,preparedStatement);
        }
    }

    /**
     * 增、删、改操作通用方法测试
     */
    @Test
    public void updatedDLTest(){
        String sql="update  user set name=? where id=? ";
        updatedDL(sql,"张四",7);//注意入参顺序
    }
}
