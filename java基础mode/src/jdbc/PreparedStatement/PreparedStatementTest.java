package jdbc.PreparedStatement;


import jdbc.获取数据库连接.Connertion;
import jdbc.config.JDBCUtils;
import jdbc.entity.User;
import org.junit.Test;

import java.io.InputStream;
import java.sql.*;
import java.util.Date;
import java.util.Properties;

/**
 * 使用PrepatedStatement来代替Statement，实现增删改查
 * 在 java.sql 包中有 3 个接口分别定义了对数据库的调用的不同方式：
 *      Statement：用于执行静态 SQL 语句并返回它所生成结果的对象。
 *      PrepatedStatement：SQL 语句被预编译并存储在此对象中，可以使用此对象多次高效地执行该语句。
 *      CallableStatement：用于执行 SQL 存储过程；
 *    使用Statement操作数据表存在弊端：（所以使用PrepatedStatement）
 *      问题一：存在拼串操作，繁琐
 *      问题二：存在SQL注入问题
 */
public class PreparedStatementTest {

    /**
     * 1.添加记录
     */
    @Test
    public void testInsert()  {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            //1.加载配置文件ClassLoader()类加载器
            InputStream stream = Connertion.class.getClassLoader().getResourceAsStream("jdbc.properties");
            Properties properties=new Properties();
            // load()加载
            properties.load(stream);
            //2.读取配置信息
            String user = properties.getProperty("user");
            String url = properties.getProperty("url");
            String password = properties.getProperty("password");
            String driverClass = properties.getProperty("driverClass");
            /**
             * Connection（数据库连接）需要四步
             *  1.Driver API
             *  2.url
             *  3.user
             *  4.password
             */
            //3.加载驱动;
            Class.forName(driverClass);
            //4.获取数据库连接;
            connection = DriverManager.getConnection(url, user, password);
            //5.sql预编译;
            String sql="insert into  user(id,name,age,email)values(?,?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            //填充占位符
            preparedStatement.setInt(1,7);
            preparedStatement.setString(2,"哪吒");
            preparedStatement.setString(3,"12");
            preparedStatement.setString(4,"962003298@qq.com");
            //6.sql执行;
            preparedStatement.execute();
            System.out.println("执行成功！！！！！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //7.资源关闭
            try {
                if(preparedStatement!=null)
                   preparedStatement.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                if(connection!=null)
                   connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }
    /**
     * 2.使用JDBCUtils工具类做添加操作
     */
    @Test
    public void testInsert2() throws Exception {
        Connection connection = null;
        PreparedStatement prepareStatement = null;
        try {
            //1.获取数据库连接
            connection = JDBCUtils.getConnection();
            //2.编译sql
            String sql="insert into  user(id,name,age,email)values(?,?,?,?)";
            prepareStatement = connection.prepareStatement(sql);
            //3.填充占位符
            prepareStatement.setInt(1,8);
            prepareStatement.setString(2,"M磨完");
            prepareStatement.setString(3,"14");
            prepareStatement.setString(4,"962003298@qq.com");
            //4.执行sql
            prepareStatement.execute();
            System.out.println("执行成功！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5.关闭资源
            JDBCUtils.closeResource(connection,prepareStatement);
        }

    }

    /**
     * 修改记录
     */
    @Test
    public void testUpdate(){
        Connection connertion=null;
        PreparedStatement preparedStatement=null;
        try {
            //1.获取数据库连接
            connertion = JDBCUtils.getConnection();
            //2.预编译sql
            String sql="update  user set name=? where id=? ";
            preparedStatement = connertion.prepareStatement(sql);
            //3.填充占位符
            preparedStatement.setString(1,"张三");
            preparedStatement.setInt(2,7);
            //4.执行sql
            preparedStatement.execute();
            System.out.println("执行成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            JDBCUtils.closeResource(connertion,preparedStatement);
        }

    }



    /**
     * 查询记录
     */
    @Test
    public void testSelect() throws Exception {
      Connection connection=null;
      PreparedStatement preparedStatement=null;
      ResultSet rs = null;
        try {
            //1.获取数据库连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            String sql="select * from user where name=?";
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            preparedStatement.setString(1,"张四");
            // 4.执行executeQuery(),得到结果集：ResultSet
            rs=preparedStatement.executeQuery();
            if(rs.next()){ //判断是否有下一个数据，没有就falset
                Long id = rs.getLong(1);
                String name = rs.getString(2);
                int age = rs.getInt(3);
                String email=rs.getString(4);
                Date create_time=rs.getDate(5);
                Date update_time=rs.getDate(6);
                int version = rs.getInt(7);
                Boolean deleted=rs.getBoolean(8);
                User user = new User(id, name, age, email, create_time, update_time,version,deleted);
                System.out.println(user.toString());
            }
            System.out.println("执行成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }finally{

            JDBCUtils.closeResource(connection,preparedStatement,rs);
        }
    }

}
