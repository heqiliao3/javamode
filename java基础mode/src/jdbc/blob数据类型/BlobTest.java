package jdbc.blob数据类型;

import jdbc.config.JDBCUtils;
import jdbc.entity.Customers;
import org.junit.Test;

import java.io.*;
import java.sql.*;

/**
 * 用PreparedStatement操作blob数据；
 */
public class BlobTest {

    /**
     * 插入blob数据
     */
    @Test
    public void test1(){
        FileInputStream fileInputStream = null;
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        try {
            connection = JDBCUtils.getConnection();
            String sql="insert into customers(name,email,photo)values(?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,"张三");
            preparedStatement.setString(2,"962003298@qq.com");
            fileInputStream = new FileInputStream(new File("zhuyin.jpg"));
            preparedStatement.setBlob(3,fileInputStream);
            preparedStatement.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,preparedStatement);
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    /**
     *查看blob数据
     */
     @Test
     public void test2(){

         InputStream binaryStream=null;
         FileOutputStream fos =null;
         PreparedStatement preparedStatement = null;
         ResultSet resultSet = null;
         Connection connection =null;

         try {
             connection = JDBCUtils.getConnection();
             String sql="select * from customers where id=?";
             preparedStatement = connection.prepareStatement(sql);
             preparedStatement.setInt(1,2);
             resultSet = preparedStatement.executeQuery();
             if (resultSet.next()){
                 Customers customers = new Customers();
                 customers.setName(resultSet.getString("name"));
                 customers.setEmail(resultSet.getString("email"));
                 Blob photo = resultSet.getBlob("photo");
                 binaryStream= photo.getBinaryStream();
                 fos= new FileOutputStream("zhu.jpg");
                 byte[] buffer =new byte[1024];
                 int len;
                 while ((len=binaryStream.read())!=-1){
                     fos.write(buffer,0,len);
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
         } finally {
             JDBCUtils.closeResource(connection,preparedStatement,resultSet);
             try {
                 if(binaryStream!=null)
                  binaryStream.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
             try {
                 if(fos!=fos)
                   fos.flush();//排出流中所有数据
                   fos.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }

     }

}
