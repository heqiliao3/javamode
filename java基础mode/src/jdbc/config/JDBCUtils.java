package jdbc.config;

import jdbc.获取数据库连接.Connertion;

import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * 操作数据库的工具类
 *
 */
public class JDBCUtils {

    /**
     * 获取数据库连接
     * @return
     * @throws Exception
     */
    public static Connection getConnection() throws Exception {
        //1.加载配置文件ClassLoader()类加载器
        InputStream stream = Connertion.class.getClassLoader().getResourceAsStream("jdbc.properties");
        Properties properties=new Properties();
        // load()加载
        properties.load(stream);
        //2.读取配置信息
        String user = properties.getProperty("user");
        String url = properties.getProperty("url");
        String password = properties.getProperty("password");
        String driverClass = properties.getProperty("driverClass");
        /**
         * Connection（数据库连接）需要四步
         *  1.Driver API
         *  2.url
         *  3.user
         *  4.password
         */
        //3.加载驱动;
        Class.forName(driverClass);
        //4.获取数据库连接;
        Connection connection = DriverManager.getConnection(url, user, password);
        return connection;
    }

    /**
     * 关闭连接资源
     */
    public static void closeResource(Connection con, Statement statement){
        try {
            if(statement!=null)
                statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            if(con!=null)
                con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 关闭连接资源
     */
    public static void closeResource(Connection con, Statement statement, ResultSet rs){
        try {
            if(statement!=null)
                statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            if(con!=null)
                con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        try {
            if(rs!=null)
                rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


}
