package jdbc.批量插入;

import jdbc.config.JDBCUtils;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 * 批量插入
 *      当需要成批插入或者更新记录时，可以采用Java的批量更新机制，这一机制允许多条语句一次性提交给数据库批量处
 *      理。通常情况下比单独提交处理更有效率
 * JDBC的批量处理语句包括下面三个方法：
 *      addBatch(String)：添加需要批量处理的SQL语句或是参数；
 *      executeBatch()：执行批量处理语句；
 *      clearBatch():清空缓存的数据
 * 通常我们会遇到两种批量执行SQL语句的情况：
 *      多条SQL语句的批量处理；
 *       一个SQL语句的批量传参；
 *
 * 举例：向数据表中插入20000条数据
 *    数据库中提供一个goods表。创建如下：
 *    CREATE TABLE goods(
 *      id INT PRIMARY KEY AUTO_INCREMENT,
 *      NAME VARCHAR(20)
 *      );
 */
public class BatchInsert {
    /**
     * 实现层次一：使用Statement
     */
    @Test
    public void test1() throws Exception {
        long start = System.currentTimeMillis();
        Connection conn = JDBCUtils.getConnection();
        Statement st = conn.createStatement();
        for (int i = 0; i <=20000 ; i++) {
            String sql = "insert into goods(name) values('name_"+i+"')";
            st.executeUpdate(sql);

        }
        long end = System.currentTimeMillis();
        System.out.println("花费的时间为：" + (end - start));//10874
        JDBCUtils.closeResource(conn, st);
    }

    /**
     * 实现层次二：使用PreparedStatement
     */
    @Test
    public void test2() throws Exception {

        long start = System.currentTimeMillis();
        PreparedStatement preparedStatement=null;
        Connection connection = JDBCUtils.getConnection();
        String sql = "insert into goods(name) values(?)";
        preparedStatement= connection.prepareStatement(sql);
        for (int i = 0; i <=20000 ; i++) {
            preparedStatement.setString(1,"name"+i);
        }
        preparedStatement.execute();
        long end = System.currentTimeMillis();
        System.out.println("花费的时间为：" + (end - start));//973
        JDBCUtils.closeResource(connection, preparedStatement);
    }

    /**
     * 实现层次三：使用PreparedStatement
     *    使用 addBatch() / executeBatch() / clearBatch()
     *    利用addBatch()攒sql执行,
     */
    @Test
    public void test3() throws Exception {

        long start = System.currentTimeMillis();
        PreparedStatement preparedStatement=null;
        Connection connection = JDBCUtils.getConnection();
        String sql = "insert into goods(name) values(?)";
        preparedStatement= connection.prepareStatement(sql);
        for (int i = 0; i <=20000 ; i++) {
            preparedStatement.setString(1,"name"+i);

            //1.攒sql
            preparedStatement.addBatch();
            if(i%1000==0){
                //2.执行
                preparedStatement.execute();
                //3.清空
                preparedStatement.clearBatch();
            }

        }

        long end = System.currentTimeMillis();
        System.out.println("花费的时间为：" + (end - start));//1049
        JDBCUtils.closeResource(connection, preparedStatement);
    }

    /**
     * 实现层次四：在层次三的基础上操作
     * 使用Connection 的 setAutoCommit(false) / commit()
     *  mysql默认是自动提交的,层次三的攒sql就是说会执行500条就提交一次浪费了时间,所以改成手动提交,节约提交所浪费的时间;
     */
    @Test
    public void test4() throws Exception {

        long start = System.currentTimeMillis();
        PreparedStatement preparedStatement=null;
        Connection connection = JDBCUtils.getConnection();
        //1.设置为不自动提交数据(mysql自动提交默认是true)
        connection.setAutoCommit(false);

        String sql = "insert into goods(name) values(?)";
        preparedStatement= connection.prepareStatement(sql);
        for (int i = 0; i <=20000 ; i++) {
            preparedStatement.setString(1,"name"+i);

            //1.攒sql
            preparedStatement.addBatch();
            if(i%1000==0){
                //2.执行
                preparedStatement.execute();
                //3.清空
                preparedStatement.clearBatch();
            }

        }
         //2.提交数据
        connection.commit();
        long end = System.currentTimeMillis();
        System.out.println("花费的时间为：" + (end - start));//977
        JDBCUtils.closeResource(connection, preparedStatement);
    }
}
