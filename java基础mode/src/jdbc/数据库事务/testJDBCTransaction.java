package jdbc.数据库事务;

import jdbc.config.JDBCUtils;
import jdbc.entity.User;
import org.junit.Test;

import java.lang.reflect.Field;
import java.sql.*;

/**
 * 事务：一组逻辑操作单元,使数据从一种状态变换到另一种状态。
 * 事务处理（事务操作）：保证所有事务都作为一个工作单元来执行，即使出现了故障，都不能改变这种执行方
 *      式。当在一个事务中执行多个操作时，要么所有的事务都被提交(commit)，那么这些修改就永久地保存下来；
 *      要么数据库管理系统将放弃所作的所有修改，整个事务回滚(rollback)到最初状态。
 *      为确保数据库中数据的一致性，数据的操纵应当是离散的成组的逻辑单元：当它全部完成时，数据的一致性可
 *      以保持，而当这个单元中的一部分操作失败，整个事务应全部视为错误，所有从起始点以后的操作应全部回退
 *      到开始状态。
 */
public class testJDBCTransaction {
    /**
     * 数据库事务的隔离级别
     * @throws Exception
     */
    @Test
  public void test1() throws Exception {
        Connection connection = JDBCUtils.getConnection();
        //关闭自动提交
        connection.setAutoCommit(false);
        //获取数据库的隔离级别
        System.out.println(connection.getTransactionIsolation());
        //设置数据库隔离级别
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
        String sql="select version from user where name=?";
        Select2(connection,User.class,sql,"刘强东");

    }

    @Test
    public void test2() throws Exception {

        Connection connection = JDBCUtils.getConnection();
        //关闭自动提交
        connection.setAutoCommit(false);
        //获取数据库的隔离级别
        System.out.println(connection.getTransactionIsolation());
        //设置数据库隔离级别
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
        String sql="UPDATE user SET version=2 WHERE NAME=?";
        updatedDL(connection,sql,"刘强东");
        Thread.sleep(15000);
        System.out.println("修改结束");
    }

    /**
     * 3.//通用的增、删、改操作（体现一：增、删、改 ； 体现二：针对于不同的表）
     */
    public void updatedDL(Connection connection,String sql,Object... args){
        PreparedStatement preparedStatement=null;
        try {
            //1.获取数据库的连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i <args.length ; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            //4.执行sql
            /**
             *  方式一：execute返回boolean:
             *         如果第一个结果是ResultSet对象，则为true；如果第一个结果是更新计数或没有结果，则为false
             */
            //preparedStatement.execute();
            /**
             * 方式二：executeUpdate返回int
             *       （1）SQL数据操作语言（DML）语句的行数，或（2）不返回任何内容的SQL语句的行数为0
             */
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResource(null,preparedStatement);
        }
    }

    /**
     * 通用的查询操作 Class<T> clazz （任意）实体，查询一条数据
     */
    public <T> T Select2(Connection connection,Class<T> clazz, String sql, Object... args) {
        PreparedStatement preparedStatement=null;
        ResultSet rs=null;
        try {
            //1.获取数据库连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            // 4.执行executeQuery(),得到结果集：ResultSet  ResultSet没有办法获取结果的数量，只有用ResultSetMetaData才能获取到;
            rs=preparedStatement.executeQuery();
            //5.得到结果集的元数据： ResultSetMetaData,通过元数据获取结果的数量
            ResultSetMetaData rsmd = rs.getMetaData();
            //6.通过ResultSetMetaData得到columnCount(数量);
            int columnCount = rsmd.getColumnCount();
            if(rs.next()){
                T t = clazz.newInstance();
                for (int i = 0; i <columnCount; i++) { //遍历每一行
                    //获取列值
                    Object columnVal = rs.getObject(i + 1);
                    //获取列明
                    String columnName = rsmd.getColumnLabel(i + 1);
                    //7.2使用反射，给对象的相应属性赋值
                    Field field = clazz.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(t,columnVal);
                }
                System.out.println("执行成功:t"+t);
                return t;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            JDBCUtils.closeResource(null,preparedStatement,rs);
        }
        return null;
    }

}
