package jdbc.获取数据库连接;


import org.junit.Test;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 获取数据库连接
 */
public class Connertion {
    /**
     * 第一种方式
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
        Driver driver=new com.mysql.cj.jdbc.Driver();
        String url="jdbc:mysql://localhost:3306/mybatis_plus?serverTimezone=GMT%2B8";
        //将用户名和密码封装到Properties中
        Properties info=new Properties();
        info.setProperty("user","root");
        info.setProperty("password","123456");
        //connect()获取数据库连接
        Connection connect = driver.connect(url,info);
        System.out.println(connect);

    }

    /**
     * 方式二:对方式一的迭代:在如下的程序中不出现第三方的api，使得程序具有更好的可移植性
     */
    @Test
    public void test2() throws Exception{
        //1.使用反射，获取Driver实现类对象
        Class aClass = Class.forName("com.mysql.cj.jdbc.Driver");
        Driver driver = (Driver)aClass.newInstance();
        //2.提供要连接的数据库
        String url="jdbc:mysql://localhost:3306/mybatis_plus?serverTimezone=GMT%2B8";
        //3.将用户名和密码封装到Properties中
        Properties info=new Properties();
        info.setProperty("user","root");
        info.setProperty("password","123456");
        //4.获取连接
        Connection connect = driver.connect(url, info);
        System.out.println("数据库连接:::"+connect);
    }

    /**
     * 方式三:DriverManager替换Driver
     */
    @Test
    public void test3() throws Exception{
        //1.使用反射，获取Driver实现类对象
        Class aClass = Class.forName("com.mysql.cj.jdbc.Driver");
        Driver driver = (Driver)aClass.newInstance();
        //2.提供另外三个连接的基本信息
        String url="jdbc:mysql://localhost:3306/mybatis_plus?serverTimezone=GMT%2B8";
        String user="root";
        String password="123456";
        //注册驱动
        DriverManager.registerDriver(driver);
        //3.获取连接
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println("数据库连接:::"+connection);
    }

    /**
     * 因为Driver帮忙做了这两步，所以不需要这两部了
     *  Driver类里面有个静态代码块:静态代码块在类加载的时候执行，所以当加载Driver类的时候他已经帮我们做了这两部(代码如下)
     * static {
     *         try {
     *             DriverManager.registerDriver(new Driver());
     *         } catch (SQLException var1) {
     *             throw new RuntimeException("Can't register driver!");
     *         }
     *     }
     * 方式四:第三个方式的优化
     */
    @Test
    public void test4() throws Exception{
        //1.提供另外三个连接的基本信息
        String url="jdbc:mysql://localhost:3306/mybatis_plus?serverTimezone=GMT%2B8";
        String user="root";
        String password="123456";

        //2.使用反射，获取Driver实现类对象
        Class.forName("com.mysql.cj.jdbc.Driver");
//        Driver driver = (Driver)aClass.newInstance();
//        //注册驱动
//        DriverManager.registerDriver(driver);
        //3.获取连接
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println("数据库连接:::"+connection);
    }

    /**
     *  方式五(最终版)：将数据库连接需要的4个基本信息声明在配置文件中，通过读取配置文件的方法，获取连接
     *    java 读取配置文件的五种方式：
     *      1.ServletContext获取真实路径
     *       String realPath = request.getServletContext().getRealPath("config.properties");
     *      2.通过ResourceBundle类获取配置文件资源
     *         配置文件放在resource源包下，不用加后缀
     *         PropertiesUtil.getProfileByResourceBundle("config");
     *      3.ClassLoader方式读取
     *         InputStream stream = Demo.class.getClassLoader().getResourceAsStream("config.properties");
     *      4.使用Sping提供的PropertiesLoaderUtils类
     *         // 使用全限定名
     *         Properties props = PropertiesLoaderUtils.loadAllProperties("config.properties");
     *         // 获取资源
     *         String userName = props.getProperty("userName");
     *      5.@Value("${content}")方式
     *        @Value("${content}")// 使用注解获取值
     *          private String content;
     * @throws Exception
     */
    @Test
    public void test5() throws Exception{
        //1.加载配置文件ClassLoader()类加载器
        InputStream stream = Connertion.class.getClassLoader().getResourceAsStream("jdbc.properties");
        Properties properties=new Properties();
        // load()加载
        properties.load(stream);
        //2.读取配置信息
        String user = properties.getProperty("user");
        String url = properties.getProperty("url");
        String password = properties.getProperty("password");
        String driverClass = properties.getProperty("driverClass");
        /**
         * Connection（数据库连接）需要四步
         *  1.Driver API
         *  2.url
         *  3.user
         *  4.password
         */
         //3.加载驱动
        Class.forName(driverClass);
        //4.获取连接
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println("数据库连接:::"+connection);

    }

}
