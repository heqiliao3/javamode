package jdbc.ApacheDBUtils;

import jdbc.config.JDBCUtils;
import jdbc.entity.Customers;
import jdbc.数据库连接池.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Apache-DBUtils
 *   commons-dbutils 是 Apache 组织提供的一个开源 JDBC工具类库，它是对JDBC的简单封装，学习成本极低，
 *   并且使用dbutils能极大简化jdbc编码的工作量，同时也不会影响程序的性能。
 */
public class DbUtilsTest {
    /*
     * 测试查询:查询一条记录
     *
     * 使用ResultSetHandler的实现类：BeanHandler
     */
    @Test
    public void test(){
        Connection connertion = null;
        try {
            QueryRunner queryRunner = new QueryRunner();
            connertion = DruidUtils.getConnertion();
            String sql = "select * from customers where id = ?";
            BeanHandler<Customers> beanHandler = new BeanHandler<>(Customers.class);
            Customers customer = queryRunner.query(connertion, sql, beanHandler, 1);
            System.out.println(customer);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connertion, null);
        }

    }

    /*
     * 测试查询:查询多条记录
     *
     *    /*
     * 测试查询:查询一条记录
     *
     *  使用ResultSetHandler的实现类：BeanListHandler
     */
    @Test
    public void test2(){
        Connection connertion = null;
        try {
            QueryRunner queryRunner = new QueryRunner();
            connertion = DruidUtils.getConnertion();
            String sql = "select * from customers where photo = ?";
            BeanListHandler<Customers> beanHandler = new BeanListHandler<>(Customers.class);
            List<Customers> query = queryRunner.query(connertion, sql, beanHandler,"17717551032");
            query.forEach(System.out::println);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connertion, null);
        }

    }

    // 测试添加
    @Test
    public void testInsert(){
        Connection conn = null;
        try {
            QueryRunner runner = new QueryRunner();
            conn = DruidUtils.getConnertion();
            String sql = "insert into customers(name,email,photo)values(?,?,?)";
            int count = runner.update(conn, sql, "何成飞", "he@qq.com", "17717551032");
            System.out.println("添加了" + count + "条记录");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResource(conn, null);
        }

    }
}
