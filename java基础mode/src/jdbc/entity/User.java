package jdbc.entity;


import java.util.Date;

public class User {

    private Long id;

    private String name;

    private int age;

    private String email;

    private Date create_time;

    private Date update_time;

    private int version;

    private Boolean deleted;

    public User() {
    }

    public User(Long id, String name, int age, String email, Date create_time, Date update_time, int version, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.create_time = create_time;
        this.update_time = update_time;
        this.version = version;
        this.deleted = deleted;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public int getVersion() {
        return version;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                ", version=" + version +
                ", deleted=" + deleted +
                '}';
    }
}
