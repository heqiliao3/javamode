package jdbc.练习;

import jdbc.config.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;


/**
 * 练习题1：从控制台向数据库的表customers中插入一条数据，表结构如下：
 */
public class CustomersTest {
    /**
     * @test Scanner 不能输入东西，可以换成main解决;
     * @param args
     */
    public static void main(String[] args) {
        //控制台输入...
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String name = scanner.next();
        System.out.println("请输入年龄:");
        String age = scanner.next();
        System.out.println("请输入邮箱:");
        String email = scanner.next();

        String sql="insert into user(name,age,email)values(?,?,?)";
        //调用通用增删改操作
        int i = updatedDL(sql, name, age, email);
        if (i>0){
            System.out.println("插入成功");
        }else {
            System.out.println("插入失败");
        }
    }


    /**
     * 3.//通用的增、删、改操作（体现一：增、删、改 ； 体现二：针对于不同的表）
     */
    public static int updatedDL(String sql, Object... args){
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        try {
            //1.获取数据库的连接
            connection = JDBCUtils.getConnection();
            //2.预编译sql
            preparedStatement = connection.prepareStatement(sql);
            //3.填充占位符
            for (int i = 0; i <args.length ; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }
            //4.执行sql
            /**
             *execute返回boolean;
             * 如果第一个结果是ResultSet对象，则为true；如果第一个结果是更新计数或没有结果，则为false
             * executeUpdate返回int
             *  （1）SQL数据操作语言（DML）语句的行数，或（2）不返回任何内容的SQL语句的行数为0
             */
            return preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            JDBCUtils.closeResource(connection,preparedStatement);
        }
        return 0;
    }
}
