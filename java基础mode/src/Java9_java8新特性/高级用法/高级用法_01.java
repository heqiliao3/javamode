package Java9_java8新特性.高级用法;

import Java9_java8新特性.StreamAPI.创建流_01;
import Java9_java8新特性.entiry.Author;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.stream.Stream;

public class 高级用法_01 {
    /**
     *  1.基本数据类型优化 mapToInt,mapToLong,mapToDouble,flatMapToInt,flatMapToDouble
     *  即使我们操作的是整数小数，但是实际用的都是他们的包装类。
     *  JDK5中引入的自动装箱和自动拆箱让我们在使用对应的包装类时就好像使用基本数据类型一样方便。
     *  但是你一定要知道装箱和拆箱肯定是要消耗时间的。虽然这个时间消耗很下。但是在大量的数据不断的重复装箱拆箱的时候，你就不能无视这个时间损耗了。
     */
    private static void test34() {
        List<Author> authors = 创建流_01.getAuthors();
        //map
        authors.stream()
                .map(Author::getAge)
                .forEach(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer age) {
                        System.out.println(age);
                    }
                });
        //mapToInt
        authors.stream()
                .mapToInt(author -> author.getAge())//优化
                .forEach(new IntConsumer() {
                    @Override
                    public void accept(int age) {
                        System.out.println(age);
                    }
                });
    }

    /**
     * 串行流转换为并行流
     *    parallel方法可以把串行流转换成并行流。
     */
    private static void test2() {
        //串行流
        Integer[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Stream<Integer> stream1 = Stream.of(arr);
        Integer sum1 = stream1
                .filter(num -> num > 5)
                .reduce((result, element) -> result + element)
                .get();
        System.out.println(sum1);

        //并行流
        Stream<Integer> stream2 = Stream.of(arr);
        Integer sum2 = stream2
                .parallel()//转换
                .filter(num -> num > 5)
                .reduce((result, element) -> result + element)
                .get();
        System.out.println(sum2);

    }

    /**
     * 并行流
     *    parallelStream换成并行流。
     */
    private static void test36() {
        //串行流对象
        List<Author> authors1 = 创建流_01.getAuthors();
        authors1.stream()
                .map(author -> author.getAge())
                .map(age -> age + 10)
                .filter(age -> age > 18)
                .map(age -> age + 2)
                .forEach(System.out::println);

        System.out.println("=========================");
        //并行流对象
        List<Author> authors2 = 创建流_01.getAuthors();
        authors2.parallelStream()
                .map(author -> author.getAge())
                .map(age -> age + 10)
                .filter(age -> age > 18)
                .map(age -> age + 2)
                .forEach(System.out::println);
    }
}
