package Java9_java8新特性.Lambda;

import org.junit.Test;

import java.util.Comparator;
import java.util.function.Consumer;

/**
 * lambda 表达式的使用:(分为6种情况)
 */
public class lambda表达式的使用02 {

    // todo lambda 表达式的使用:(分为6种情况)
    //语法格式一:无参，无返回值
    @Test
    public void test1(){
        //之前的写法
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("我爱北京天安门");
            }
        };
        runnable.run();
        //lambda表达式写法
        Runnable runnable2=()-> System.out.println("我爱北京天安门");
        runnable2.run();

    }
    //语法格式二:lambda 需要一个参数，但是没有返回值;
    @Test
    public void test2(){
        System.out.println("***************************之前的写法*********************************");
        Consumer<String> con = new Consumer<String>(){
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };
        con.accept("张三");
        System.out.println("***************************lambda 表达式实现方式*********************************");
        Consumer<String> con2 = (String s)->{System.out.println(s);};
        con2.accept("李四");
    }
    //语法格式三:类型推断;(String s)-> 变成(s),类型是根据Consumer<String>推断出来的
    @Test
    public void test3(){
        System.out.println("***************************lambda 表达式实现方式*********************************");
        Consumer<String> con2 = (String s)->{System.out.println(s);};
        con2.accept("李四");

        System.out.println("***************************优化*********************************");
        Consumer<String> con1 = (s)->System.out.println(s);
        con1.accept("李四");
    }

    //语法格式四:lambda 若只需要一个参数时，参数的小括号可以省略 (s)->变成s->
    @Test
    public void test4(){
        System.out.println("***************************优化*********************************");
        Consumer<String> con1 = s->System.out.println(s);
        con1.accept("李四");
    }

    //语法格式五:lambda 需要两个或以上的参数，多条执行语句，并且可以有返回值
    @Test
    public void test6(){
        System.out.println("***************************老的实现方法*******************************");
        Comparator<Integer> objectComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                System.out.println(01);
                System.out.println(02);
                return Integer.compare(o1, o2);
            }

        };
        int compare = objectComparator.compare(25, 12);
        System.out.println(compare);
        System.out.println("***************************lambda 表达式实现方式*********************************");
        Comparator<Integer> comparator6=(o1, o2)->{
            System.out.println(01);
            System.out.println(02);
            return Integer.compare(o1, o2);
        };
        int compare1 = comparator6.compare(12,25);
        System.out.println(compare1);
    }
}
