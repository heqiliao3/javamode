package Java9_java8新特性.Lambda;


import org.junit.Test;

import java.util.Comparator;

/**
 * lambda 表达式学习
 *  1.举例：(o1,o2)->Integer.compare(o1,o2);
 *  2.格式:
 *      ->: lambda操作符或箭头操作符
 *      ->: 左边: lambda 形参列表(其实就是接口中的抽象方法的形参列表)
 *      ->: 右边: lambda 体(其实就是重写的抽象方法的方法体)
 *  3.lambda本质:作为接口的实例
 *  二, lambda 表达式需要"函数式接口"德支持
 *   函数式接口：接口中只有一个抽象方法的接口，称之为函数式接口，可以使用注释@FunctionalInterface 修饰可以检查是否是函数式接口;
 *    例如：@FunctionalInterface
 *        public interface Comparator<T> {
 */

public class LambdaTest01 {
    //  todo lambda
    @Test
    public void test1(){
        //之前的写法
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("我爱北京天安门");
            }
        };
        runnable.run();
        //lambda表达式写法
        Runnable runnable2=()-> System.out.println("我爱北京天安门");
        runnable2.run();
    }

    @Test
    public void test2(){
        //老的实现方法
        System.out.println("***************************老的实现方法*******************************");
        Comparator<Integer> objectComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }

        };
        int compare = objectComparator.compare(25, 12);
        System.out.println(compare);
        System.out.println("***************************lambda 表达式实现方式*******************************");
        //lambda 表达式实现方式
        Comparator<Integer> comparator=(o1,o2)->Integer.compare(o1,o2);
        int compare2 = comparator.compare(12, 25);
        System.out.println(compare2);

        System.out.println("***************************方法引用的写法*******************************");
        //lambda 表达式实现方式
        Comparator<Integer> comparator3=Integer::compare;
        int compare3 = comparator.compare(12, 25);
        System.out.println(compare3);


    }
}
