package Java9_java8新特性.函数式接口;

import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * java8 内置的四大核心函数式接口
 * Consumer<T>: 消费性接口
 *      void accept(T t); 有参数没有返回值
 * Supplier<T>: 供给型接口
 *         T get(); 有返回值没有参数
 * Function<R,T>: 函数式接口(R 是返回值类型，T是参数类型)
 *         R apply<T t>; 有返回值有参数
 * Predicate<T>:断言型接口
 *         boolean test(T t);
 */
public class 四大函数式接口03 {
    // todo 四大函数式接口

    // todo Consumer<T>: 消费性接口

    /**
     * 之前的写法
     */
    @Test
    public void test1_1(){
        happy(10000, new Consumer<Double>() {
            @Override
            public void accept(Double d) {
                System.out.println("学习太累了，去天上人间买瓶矿泉水，价格为"+d+"元");
            }
        });
    }

    /**
     * lambda写法
     */
    @Test
   public void test1(){
       happy(10000,d-> System.out.println("学习太累了，去天上人间买瓶矿泉水，价格为"+d+"元"));
   }

    /**
     * 方法引用
     */
    @Test
    public void test1_2(){
        PrintStream out = System.out;
        happy(10000,out::println);
    }

    //消费方法
    public void happy(double money, Consumer<Double> consumer){
        consumer.accept(money);
    }

   // todo Supplier<T>: 供给型接口(生成十个随机数存到集合中去)
    // 需求: 生产整数并存放到集合中去;

    /**
     * 之前的写法
     */
    @Test
    public void test2_1(){
        List<Integer> numList = getNumList(10, new Supplier<Integer>() {
            @Override
            public Integer get() {
                return (int)Math.random() * 100;
            }
        });
        numList.stream().forEach(System.out::println);

    }
    /**
     * lambda写法
     */
    @Test
    public void test2(){
        List<Integer> numList = getNumList(10, () -> (int) (Math.random() * 100));
        numList.stream().forEach(System.out::println);

    }
    //参数一：随后数的个数
    public List<Integer> getNumList(int a , Supplier<Integer> supplier){
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            list.add(supplier.get());
        }
        return list;
    }
    // todo Function<R,T>: 函数式接口(R 是返回值类型，T是参数类型)
    @Test
    public void test4(){
        /**
         * 之前的写法
         */
        Integer integer = typeConver(new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return Integer.parseInt(s);
            }
        });
        System.out.println(integer);
        /**
         * lambda写法
         */
        Integer integer1 = typeConver((s) -> Integer.parseInt(s));
        System.out.println(integer1);
        /**
         * 方法引用
         */
        Integer integer2 = typeConver(Integer::parseInt);
        System.out.println(integer2);
    }
    public static <R> R typeConver(Function<String,R> function){
        String str="1235";
        R apply = function.apply(str);
       return apply;
    }

    // todo Predicate<T>:断言型接口
    /**
     * 之前的写法
     */
    @Test
    public void test3(){
        List<String> filterList2 = Arrays.asList("南京","北京","东京","河南","上海");
        filterString(filterList2, new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.contains("京");
            }
        });
    }
    /**
     * lambda写法
     */
    @Test
    public void test3_1(){
        List<String> filterList2 = Arrays.asList("南京","北京","东京","河南","上海");
        filterString(filterList2, s -> s.contains("京"));
    }
    //根据给定的规则，过滤集合中的字符串。此规则由Predicate的方法决定的
    public List<String> filterString(List<String> list, Predicate<String> pre){
        List<String> filterList = new ArrayList<>();
        for (String filter:list) {
            if (pre.test(filter)) {

                filterList.add(filter);

            }
        }
        System.out.println(filterList);
        return filterList;
    }
}
