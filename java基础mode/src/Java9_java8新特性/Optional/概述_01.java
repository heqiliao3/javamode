package Java9_java8新特性.Optional;

import Java9_java8新特性.entiry.Author;
import org.junit.Test;

import java.util.Optional;


public class 概述_01 {
    /**
     * 概述
     *   我们在编写代码的时候出现最多的就是空指针异常。所以在很多情况下我们需要做各种非空的判断
     */
    @Test
    public void test_1(){
        Author author = getAuthor();
        if(author!=null){
            System.out.println(author.getName());
        }

    }

    /**
     * Optional 方式
     */
    @Test
    public void test_2(){
        Author author = getAuthor();
        Optional<Author> author1 = Optional.ofNullable(author);
        author1.ifPresent(author2 -> System.out.println(author2));

    }

    /**
     * Optional 方式
     */
    @Test
    public void test_3(){
        Optional<Author> author21 = getAuthor2();
        author21.ifPresent(author2 -> System.out.println(author2.getName()));

    }

    public static Author getAuthor() {
        Author author = new Author(1L, "蒙多", 33, "一个从菜刀中明悟真理的祖安人", null);
        return author;
    }

    public static Optional<Author> getAuthor2() {
        Author author = new Author(1L, "蒙多", 33, "一个从菜刀中明悟真理的祖安人", null);
        return Optional.ofNullable(author);
    }
}
