package Java9_java8新特性.Optional;

import Java9_java8新特性.StreamAPI.创建流_01;
import Java9_java8新特性.entiry.Author;
import Java9_java8新特性.entiry.Book;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class 使用_02 {

    /**
     * 1.创建对象
     *   ofNullable方法
     *   ifPresent方法:判断是否为null
     */
    @Test
    public void test_1(){
        Author author = getAuthor();
        Optional<Author> author1 = Optional.ofNullable(author);
        author1.ifPresent(author2 -> System.out.println(author2));

    }

    /**
     * 1.创建对象
     * Optional 方式
     */
    @Test
    public void test_2(){
        Optional<Author> author21 = getAuthor2();
        author21.ifPresent(author2 -> System.out.println(author2.getName()));

    }
    /**
     * 1.创建对象
     * Optional 方式
     * of方法 :如果你确定一个对象不是空的则可以使用Optional的静态方法of来把数据封装成Optional对象
     */
    @Test
    public void test_3(){
        Author author22 = getAuthor();
        Optional<Author> author221 = Optional.of(author22);
        author221.ifPresent(author2 -> System.out.println(author2.getName()));

    }

    /**
     * 3.获取值
     * get方法 如果我们想获取值自己进行处理可以使用get方法获取，但是不推荐。因为当Optional内部的数据为空的时候会出现异常
     */
    @Test
    public void test2_1(){
        Author author22 = getAuthor();
        Optional<Author> author221 = Optional.ofNullable(author22);
        Integer age = author221.get().getAge();
        System.out.println(age);

    }

    /**
     * 4 安全获取值
     * orElseGet 获取数据并且设置数据为空时的默认值。如果数据不为空就能获取到该数据。如果为空则根据你传入的参数来创建对象作为默认值返回。
     */
    @Test
    public void test4_1(){
        Optional<Author> author21 = getAuthor2();
        Author author = author21.orElseGet(()->new Author());
        System.out.println(author);

    }
    /**
     * 4 安全获取值
     * orElseThrow 获取数据，如果数据不为空就能获取到该数据。如果为空则根据你传入的参数来创建异常抛出。
     */
    @Test
    public void test4_2(){
        Optional<Author> author21 = getAuthor2();
        try {
            Author author = author21.orElseThrow(()->new RuntimeException("数据为null"));
            System.out.println(author);
        }catch (RuntimeException e){
            e.printStackTrace();
        }


    }

    /**
     * 5 过滤
     * filter 我们可以使用filter方法对数据进行过滤。如果原本是有数据的，但是不符合判断，也会变成一个无数据的Optional对象。
     */
    @Test
    public void test5(){
        Optional<Author> author21 = getAuthor2();
        author21
                .filter(author -> author.getAge()>18)
                .ifPresent(author -> System.out.println(author.getName()));

    }

    /**
     * 6 判断
     * isPresent 我们可以使用isPresent方法进行是否存在数据的判断。如果为空返回值为false，如果不为空，返回值为true。
     *           但是这种方式并不能体现Optional的好处，更推荐使用ifPresent方法。
     */
    @Test
    public void test6(){
        Optional<Author> author21 = getAuthor2();
        if (author21.isPresent()) {
            System.out.println(author21.get().getName());
            System.out.println(author21.get().getAge());
        }
    }

    /**
     * 7 数据转换
     *  Optional还提供了map可以让我们对数据进行转换，并且转换得到的数据也是被Optional包装好的，保证了我们的使用安全。
     */
    @Test
    public void test7(){
        Optional<Author> author21 = getAuthor2();
        author21.map(author -> author.getBooks())
                .ifPresent(bookList-> bookList.stream()
                                             .forEach(book -> System.out.println(book.getName())));
    }



    public static Author getAuthor() {
        Author author = new Author(1L, "蒙多", 33, "一个从菜刀中明悟真理的祖安人", null);
        return null;
    }

    public static Optional<Author> getAuthor2() {
        //数据初始化
        Author author = new Author(1L, "蒙多", 33, "一个从菜刀中明悟哲理的祖安人", null);
        Author author2 = new Author(2L, "亚拉索", 15, "狂风也追逐不上他的思考速度", null);
        Author author3 = new Author(3L, "易", 14, "是这个世界在限制他的思维", null);
        Author author4 = new Author(3L, "易", 14, "是这个世界在限制他的思维", null);

        //书籍列表
        List<Book> books1 = new ArrayList<>();
        List<Book> books2 = new ArrayList<>();
        List<Book> books3 = new ArrayList<>();

        books1.add(new Book(1L, "刀的两侧是光明与黑暗", "哲学,爱情", 88, "用一把刀划分了爱恨"));
        books1.add(new Book(2L, "一个人不能死在同一把刀下", "个人成长,爱情", 99, "讲述如何从失败中明悟真理"));

        books2.add(new Book(3L, "那风吹不到的地方", "哲学", 85, "带你用思维去领略世界的尽头"));
        books2.add(new Book(3L, "那风吹不到的地方", "哲学", 85, "带你用思维去领略世界的尽头"));
        books2.add(new Book(4L, "吹或不吹", "爱情,个人传记", 56, "一个哲学家的恋爱观注定很难把他所在的时代理解"));

        books3.add(new Book(5L, "你的剑就是我的剑", "爱情", 56, "无法想象一个武者能对他的伴侣这么的宽容"));
        books3.add(new Book(6L, "风与剑", "个人传记", 100, "两个哲学家灵魂和肉体的碰撞会激起怎么样的火花呢？"));
        books3.add(new Book(6L, "风与剑", "个人传记", 100, "两个哲学家灵魂和肉体的碰撞会激起怎么样的火花呢？"));

        author.setBooks(books1);
        author2.setBooks(books2);
        author3.setBooks(books3);
        author4.setBooks(books3);

        List<Author> authorList = new ArrayList<>(Arrays.asList(author, author2, author3, author4));
        return Optional.ofNullable(author);
    }


}
