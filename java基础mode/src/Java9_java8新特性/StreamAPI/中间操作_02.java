package Java9_java8新特性.StreamAPI;

import Java9_java8新特性.entiry.Author;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
import static java.util.Comparator.comparingInt;

public class 中间操作_02 {
    /**
     * ① filter
     *可以对流中的元素进行条件过滤，符合过滤条件的才能继续留在流中。
     * 例如：打印所有姓名长度大于1的作家的姓名
     */
    @Test
    public void test(){
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .filter(author -> author.getName().length() > 1)
                .forEach(System.out::println);

    }

    /**
     *  ② map
     * 可以把对流中的元素进行计算或转换
     * 例如：打印所有作家的名字
     */
    @Test
    public void test2(){
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .map(author -> author.getName())
                .forEach(System.out::println);

    }

    /**
     *  ③ distinct
     * 可以去除流中的重复元素
     * 例如：打印所有作家的姓名，并且要求其中不能有重复元素
     */
    @Test
    public void test3(){
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .distinct()
                .forEach(System.out::println);

    }

    /**
     * ④ sorted
     * 可以对流中的元素进行排序s
     * 例如：对流中的元素按照年龄进行降序排序，并且要求不能有重复的元素
     */
    @Test
    public void test4(){
        //空参构造
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .distinct()
                .sorted()
                .forEach(author -> System.out.println(author.getAge()));
        //有参构造(lambda)
        authors.stream()
                .distinct()
                .sorted((o1, o2) -> o2.getAge() - o1.getAge())
                .forEach(author -> System.out.println(author.getAge()));
        //有参构造(方法引用)
        authors.stream()
                .distinct()
                .sorted(comparingInt(Author::getAge))
                .forEach(author -> System.out.println(author.getAge()));
    }
    /**
     * ⑤ limit
     * 可以设置流的最大长度，超出的部分将被抛弃
     * 例如：对流中的元素按照年龄进行降序排序，并且要求不能有重复的元素，然后打印其中年龄最大的两个作家的姓名
     */
    @Test
    public void test5(){
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .distinct()
                .sorted(comparingInt(Author::getAge))
                .limit(2)
                .forEach(author -> System.out.println(author.getName()));

    }
    /**
     * ⑥ skip
     * 跳过流中的前n个元素，返回剩下的元素
     * 例如：打印除了年龄最大的作家外的其他作家，要求不能有重复元素，并且按照年龄降序排序。
     */
    @Test
    public void test6(){
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .distinct()
                .sorted((o1, o2) -> o2.getAge() - o1.getAge())
                .skip(1)
                .forEach(author -> System.out.println(author.getName()));

    }
    /**
     *⑦ flatMap
     * map只能把一个对象转换成另一个对象来作为流中的元素。而flatMap可以把一个对象转换成多个对象作为流中的元素
     * 例一：打印所有书籍的名字。要求对重复的元素进行去重。
     * 例二：打印现有数据的所有分类。要求对分类进行去重。不能出现这样格式：哲学，爱情
     */
        @Test
        public void test7(){
            List<Author> authors = 创建流_01.getAuthors();
            //例一：打印所有书籍的名字。要求对重复的元素进行去重。
            authors.stream()
                    .flatMap(author -> author.getBooks().stream())
                    .distinct()
                    .forEach(book -> System.out.println(book.getName()));
            System.out.println("********************************************");
           //例二：打印现有数据的所有分类。要求对分类进行去重。不能出现这样格式：哲学，爱情
            authors.stream()
                    .flatMap(author -> author.getBooks().stream())
                    .distinct()
                    .flatMap(book -> Arrays.stream(book.getCategory().split(",")))
                    .distinct()
                    .forEach(c-> System.out.println(c));

        }

}
