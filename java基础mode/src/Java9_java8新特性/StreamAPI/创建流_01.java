package Java9_java8新特性.StreamAPI;

import Java9_java8新特性.entiry.Author;
import Java9_java8新特性.entiry.Book;
import org.junit.Test;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class 创建流_01 {

    /**
     * 我们可以调用`getAuthors()`方法获取到作家的集合，现在需要打印所有年龄小于18的作家的名字，并且要注意去重。
     *  ① 单列集合：集合对象.stream( );
     */
    @Test
    public void test(){
        List<Author> authors = getAuthors();
        /**
         * 之前的写法
         */
        authors.stream()
                .distinct()
                .filter(new Predicate<Author>() {
                            @Override
                            public boolean test(Author author) {
                                return author.getAge()<18;
                            }
                        })
                .forEach(new Consumer<Author>() {
                    @Override
                    public void accept(Author author) {
                        System.out.println(author.getName());
                    }
                });
        /**
         * lambda写法
         */
        authors.stream()
                .distinct()
                .filter(author -> author.getAge()<18)
                .forEach(author -> System.out.println(author.getName()));

    }

    /**
     * ② 数组
     * Arrays.stream(数组)或者使用Stream.of来创建
     */
    @Test
    public void test2(){
        Integer[] arr = {1,1, 2, 3, 4, 5};
        Stream<Integer> stream = Arrays.stream(arr);
        stream.distinct()
               .filter(s->s>2)
               .forEach(System.out::println);

    }

    /**
     * ③ 双列集合
     * 转换成单列集合后再创建
     */
    @Test
    public void test3(){
        Map<String, Integer> map = new HashMap<>();
        map.put("蜡笔小新", 5);
        map.put("zyy", 22);
        map.put("zqh", 23);
        map.entrySet()//双列集合entrySet转换成单列集合后再创建
                .stream()
                .distinct()
                .filter(s -> s.getValue() > 16)
                .forEach(System.out::println);
    }

    public static List<Author> getAuthors() {
        //数据初始化
        Author author = new Author(1L, "蒙多", 33, "一个从菜刀中明悟哲理的祖安人", null);
        Author author2 = new Author(2L, "亚拉索", 15, "狂风也追逐不上他的思考速度", null);
        Author author3 = new Author(3L, "易", 14, "是这个世界在限制他的思维", null);
        Author author4 = new Author(3L, "易", 14, "是这个世界在限制他的思维", null);

        //书籍列表
        List<Book> books1 = new ArrayList<>();
        List<Book> books2 = new ArrayList<>();
        List<Book> books3 = new ArrayList<>();

        books1.add(new Book(1L, "刀的两侧是光明与黑暗", "哲学,爱情", 88, "用一把刀划分了爱恨"));
        books1.add(new Book(2L, "一个人不能死在同一把刀下", "个人成长,爱情", 99, "讲述如何从失败中明悟真理"));

        books2.add(new Book(3L, "那风吹不到的地方", "哲学", 85, "带你用思维去领略世界的尽头"));
        books2.add(new Book(3L, "那风吹不到的地方", "哲学", 85, "带你用思维去领略世界的尽头"));
        books2.add(new Book(4L, "吹或不吹", "爱情,个人传记", 56, "一个哲学家的恋爱观注定很难把他所在的时代理解"));

        books3.add(new Book(5L, "你的剑就是我的剑", "爱情", 56, "无法想象一个武者能对他的伴侣这么的宽容"));
        books3.add(new Book(6L, "风与剑", "个人传记", 100, "两个哲学家灵魂和肉体的碰撞会激起怎么样的火花呢？"));
        books3.add(new Book(6L, "风与剑", "个人传记", 100, "两个哲学家灵魂和肉体的碰撞会激起怎么样的火花呢？"));

        author.setBooks(books1);
        author2.setBooks(books2);
        author3.setBooks(books3);
        author4.setBooks(books3);

        List<Author> authorList = new ArrayList<>(Arrays.asList(author, author2, author3, author4));
        return authorList;
    }
}
