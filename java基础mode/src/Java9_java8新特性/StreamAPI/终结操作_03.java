package Java9_java8新特性.StreamAPI;

import Java9_java8新特性.entiry.Author;
import Java9_java8新特性.entiry.Book;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class 终结操作_03 {

    /**
     * ① forEach
     * 对流中的元素进行遍历操作，我们通过传入的参数去指定对遍历到的元素进行什么具体操作
     * 例子：输出所有作家的名字
     */
    @Test
    public void test1(){
        List<Author> authors = 创建流_01.getAuthors();
        authors.stream()
                .map(author -> author.getName())
                .distinct()
                .forEach(name-> System.out.println(name));

    }
    /**
     * ② count
     * 可以用来获取当前流中元素的个数
     * 例子：打印这些作家所出书籍的数目，注意删除重复元素
     */
    @Test
    public void test2(){
        List<Author> authors = 创建流_01.getAuthors();
        long count = authors.stream()
                .flatMap(author -> author.getBooks().stream())
                .distinct()
                .count();
        System.out.println(count);

    }
    /**
     * ③ min&max
     * 可以用来获取流中的最值
     * 例子：分别获取这些作家的所出书籍的最高分和最低分并打印
     */
    @Test
    public void test3(){
        List<Author> authors = 创建流_01.getAuthors();
        Optional<Integer> max = authors.stream()
                .flatMap(author -> author.getBooks().stream())
                .map(Book::getScore)
                .distinct()
                .max((o1, o2) -> o1 - o2);
        Optional<Integer> min = authors.stream()
                .flatMap(author -> author.getBooks().stream())
                .map(Book::getScore)
                .distinct()
                .min((o1, o2) -> o1 - o2);
        System.out.println(max.get());
        System.out.println(min.get());

    }

    /**
     * ④ collect
     * 把当前流转换成一个集合
     *
     */
    @Test
    public void test4(){
        List<Author> authors = 创建流_01.getAuthors();
        //例一：获取一个存放所有作者名字的List集合
        List<String> collect = authors.stream()
                .map(author -> author.getName())
                .collect(Collectors.toList());
        System.out.println(collect);
        //例二：获取一个所有书名的Set集合
        Set<String> collect1 = authors.stream()
                .flatMap(author -> author.getBooks().stream())
                .map(book -> book.getName())
                .collect(Collectors.toSet());
        System.out.println(collect1);
       //例三：获取一个map集合，map的key为作者名，value为List
        Map<String, List<Book>> collect2 = authors.stream()
                .distinct()
                .collect(Collectors.toMap(author -> author.getName(), author -> author.getBooks()));
        System.out.println(collect2);
    }
    /**
     * ⑤ 查找与匹配
     * 1、anyMatch
     * 可以用来判断是否有任意符合匹配条件的元素，结果为boolean类型
     * 例子：判断是否有年龄在29以上的作家
     *
     */
    @Test
    public void test5_1(){
        List<Author> authors = 创建流_01.getAuthors();
        boolean b = authors.stream()
                .anyMatch(author -> author.getAge() > 29);
        System.out.println(b);
    }

    /**
     * ⑤ 查找与匹配
     *2、allMatch
     * 可以用来判断是否都符合匹配条件，结果为boolean类型。如果都符合结果为true，否者结果为false
     * 例子：判断是否所有的作家都是成年人
     *
     */
    @Test
    public void test5_2(){
        List<Author> authors = 创建流_01.getAuthors();
        boolean b = authors.stream()
                .allMatch(author -> author.getAge() > 5);
        System.out.println(b);
    }
    /**
     * ⑤ 查找与匹配
     * 3、noneMatch
     * 可以判断流中的元素是否都不符合匹配条件。如果都不符合结果为true，否则结果为false
     * 例子：判断作家是否都没有超过100岁的
     *
     */
    @Test
    public void test5_3(){
        List<Author> authors = 创建流_01.getAuthors();
        boolean b = authors.stream()
                .noneMatch(author -> author.getAge() > 100);
        System.out.println(b);
    }
    /**
     * ⑤ 查找与匹配
     * 4、finaAny
     * 获取流中的任意一个元素。该方法没有办法保证获取的一定是流中的第一个元素
     * 例子：获取任意一个大于18岁的作家，如果存在输出他的名字
     *
     */
    @Test
    public void test5_4(){
        List<Author> authors = 创建流_01.getAuthors();
        Optional<Author> any = authors.stream()
                .filter(author -> author.getAge() > 18)
                .findAny();
        System.out.println(any);
    }

    /**
     * ⑤ 查找与匹配
     * 5、findFirst
     * 获取流中的第一个元素
     * 例子：获取一个年龄最小的作家，并输出他的姓名
     */
    @Test
    public void test5_5(){
        List<Author> authors = 创建流_01.getAuthors();
        Optional<Author> any = authors.stream()
                .filter(author -> author.getAge() > 18)
                .findFirst();
        any.ifPresent(System.out::println);
    }

    /**
     ⑥ reduce归并
     对流中的数据按照你指定的计算方式计算出一个结果。（缩减操作）
     reduce的作用是把stream中的元素给组合起来，我们可以传入一个初始值，它会按照我们的计算方式依次拿流中的元素和初始化值进行计算，计算结果再和后面的元素计算。
     reduce两个参数的重载形式内部的计算方式如下：
     */
    @Test
    public void test6_1(){
        List<Author> authors = 创建流_01.getAuthors();
        Integer reduce = authors.stream()
                        .distinct()
                        .map(author -> author.getAge())
                        .reduce(0, (result, element) -> result + element);
        System.out.println(reduce);
    }
    /**
     ⑥ reduce归并
     例二：使用reduce求所有作者中年龄的最大值
     */
    @Test
    public void test6_2(){
        List<Author> authors = 创建流_01.getAuthors();
        Integer reduce = authors.stream()
                .distinct()
                .map(author -> author.getAge())
                .reduce(Integer.MIN_VALUE,(result, element)->result<element?element:result);
        System.out.println(reduce);
    }
    /**
     ⑥ reduce归并
     例三：使用reduce求所有作者中年龄的最小值
     */
    @Test
    public void test6_3(){
        List<Author> authors = 创建流_01.getAuthors();
        Integer reduce = authors.stream()
                .distinct()
                .map(author -> author.getAge())
                .reduce(Integer.MAX_VALUE,(result, element)->result<element?result:element);
        System.out.println(reduce);
    }

}
