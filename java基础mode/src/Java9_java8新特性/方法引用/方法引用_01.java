package Java9_java8新特性.方法引用;

import Java9_java8新特性.StreamAPI.创建流_01;
import Java9_java8新特性.entiry.Author;
import org.junit.Test;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * 我们在使用lambda时不需要考虑什么时候用方法引用，用哪种方法引用，方法引用的格式是什么。
 * 我们只需要在写完lambda方法发现方法体只有一行代码，
 * 并且是方法的调用时使用快捷键尝试是否能够转换成方法引用即可。
 * 当我们方法引用使用的多了慢慢的也可以直接写出方法引用。
 */
public class 方法引用_01 {

    /**
     * 1 引用静态方法->类名: :方法名
     *  如果我们在重写方法的时候，方法体中只有一行代码，
     *  并且这行代码是调用了某个类的静态方法，
     *  并且我们把要重写的抽象方法中所有的参数都按照顺序传入了这个静态方法中，
     *  这个时候我们就可以引用类的静态方法
     */
    @Test
    private static void test1() {
        List<Author> authors = 创建流_01.getAuthors();
        //优化前
        authors.stream()
                .map(author -> author.getAge())
                .filter(age -> age > 20)
                .map(new Function<Integer, String>() {
                    @Override
                    public String apply(Integer obj) {
                        return String.valueOf(obj);
                    }
                })
                .forEach(s -> System.out.println(s));

        //优化后
        authors.stream()
                .map(Author::getAge)
                .filter(age -> age > 20)
                .map(String::valueOf)
                .forEach(System.out::println);
    }
    /**
     * 2 引用类的实例方法->对象名 ::方法名
     *  如果我们在重写方法的时候，方法体中只有一行代码，并且这行代码是调用了第一个参数的成员方法，
     *  并且我们把要重写的抽象方法中剩余的所有的参数都按照顺序传入了这个成员方法中，这个时候我们就可以引用类的实例方法。
     */
    @Test
    private static void test2() {
        List<Author> authors = 创建流_01.getAuthors();
        StringBuilder sb = new StringBuilder();
        //优化前
        authors.stream()
                .map(author -> author.getName())
                .forEach(new Consumer<String>() {
                    @Override
                    public void accept(String name) {
                        sb.append(name);
                    }
                });

        //优化后
        authors.stream()
                .map(author -> author.getName())
                .forEach(sb::append);

        System.out.println(sb.toString());
    }

    /**
     * 3. 构造器引用->类名 : : new
     *
     * 果我们在重写方法的时候，方法体中只有一行代码，并且这行代码是调用了某个类的构造方法，并且我们把要重写的抽象方法中的所有的参数都按照顺序传入了这个构造方法中，这个时候我们就可以引用构造器。
     */
    private static void test3() {
        List<Author> authors = 创建流_01.getAuthors();
        //优化前
        authors.stream()
                .map(new Function<Author, String>() {
                    @Override
                    public String apply(Author author) {
                        return author.getName();
                    }
                })
                .map(new Function<String, StringBuilder>() {
                    @Override
                    public StringBuilder apply(String name) {
                        return new StringBuilder(name);
                    }
                })
                .map(new Function<StringBuilder, String>() {
                    @Override
                    public String apply(StringBuilder sb) {
                        return sb.append("三更").toString();
                    }
                })
                .forEach(new Consumer<String>() {
                    @Override
                    public void accept(String name) {
                        System.out.println(name);
                    }
                });

        //优化后
        authors.stream()
                .map(Author::getName)
                .map(StringBuilder::new)
                .map(sb -> sb.append("三更").toString())
                .forEach(System.out::println);
    }
}
