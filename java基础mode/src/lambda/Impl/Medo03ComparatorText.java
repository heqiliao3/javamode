package lambda.Impl;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Comparator:比较功能，对一些对象的集合施加了一个整体排序;
 * Arrays:该类包含用于操作数组的各种方法（如排序和搜索）。
 *      sort(T[] a, Comparator<? super T> c) :根据指定的比较器引发的顺序对指定的对象数组进行排序。
 *
 *   lambda表达式返回值形式案例
 * 案例说明：如果一个方法的返回值类型是一个函数式接口，那么就可以直接返回一个lambda表达式；
 * 当需要通过一个java.util.Comparator接口类型的对象作为排序器时：就可以调用该方法获取；
 */
public class Medo03ComparatorText {
    public static Comparator<String> getComparator(){

        //原始写法
      /*  return new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                //按字符串长度进行降序
                return o2.length()-o1.length();
            }
        };*/
      //lambda表达式
      return (String o1, String o2)-> o2.length()-o1.length();
    };

    public static void main(String[] args) {
       //创建一个数组
        String[] a={"aaaa","vv","ccccc","dddddd"};
        //把数组转换成字符串
        System.out.println(Arrays.toString(a));
        //调用arrays中的sort方法，对字符串数组进行排序
        Arrays.sort(a,getComparator());
        //重新排序的数组
        System.out.println(Arrays.toString(a));
    }
}
