package lambda.Impl;

import lambda.MyfunctionalInterface;

/**
 * 函数式接口的使用：一般可以作为一个方法的参数和返回值类型使用
 */
public class Medo01 {

    public static void  show(MyfunctionalInterface myfunctionalInterface){
        myfunctionalInterface.method();
    }

    public static void main(String[] args) {
        //show(new MyfunctionalInterface());

        //调用show方法，方法的参数是一个接口，所以我们可以传递接口的匿名内部类
        show(new MyfunctionalInterface() {
            @Override
            public void method() {
                System.out.println("使用匿名内部类重写接口中的抽象方法");
            }
        });
        //调用show方法，方法的参数是一个接口，所以我们可以传递接口的匿名内部类
        show(()->{
            System.out.println("使用lambda表达式重写接口的抽象方法");
        });
        //lambda表达式简化版
        show(()->System.out.println("使用lambda表达式简化版"));
    }

}
