package lambda.Impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Predicate 函数接口实现 接受一个输入参数，返回一个布尔值结果。
 *     test 方法会对输入的参数做一个评判
 *  Arrays 该类包含用于操作数组的各种方法，该类还包含一个静态工厂，可以将数组视为列表。
 *      asList把数组转换成list
 */
public class Medo02PredicateTest {

    public static void main(String[] args) {
     List<Integer> list= Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println("输出所有数据:");
        eval(list, m-> true);
        System.out.println("输出所有偶数:");
        eval(list, m-> m%2==0);
        System.out.println("输出大于 3 的所有数字:");
        eval(list, n-> n > 3 );
    }

    public  static void  eval(List<Integer> list, Predicate<Integer> predicate){
        for (Integer n:list){
            if(predicate.test(n)){
                System.out.println(n+"");
            }
        }

    }
}
