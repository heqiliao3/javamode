package lambda;

/**
 * 函数接口：有且只有一个抽象方法的接口，称之为函数接口
 * 当然接口中可以包含其他的方法（默认，静态，私有）
 *
 * @FunctionalInterface注解
 *  作用：可以检测接口是否是一个函数式接口；
 *    是：编译成功；
 *    否：编译失败（接口中没有抽象方法。或者抽象方法的个数多余1个）
 */
@FunctionalInterface
public interface MyfunctionalInterface {
    public abstract  void  method();
    //默认方法
    default void doSomeMoreWork1()
    {
        // Method body
    };
//    //私有方法
//    private void doSomeMoreWork2()
//    {
//        // Method body
//    };
//    //静态方法
//    private static void doSomeMoreWork3()
//    {
//        // Method body
//    };


}
