package Java8_反射;

public class Java02_类加载器 {
    public static void main(String[] args) {
        // todo 反射-类加载器
        // 加载类
        // java中的类主要分为3种：
        // 1.Java类库中的类：String object
        // 2.JVM软件平台开发商
        // 3.自己写的类User
        // todo 加载器也有三种
        //1.BootClassloader :启动类加载器(加载类时,采用操作系统平台语言实现)
        //2.PlatformClassLoader:平台类加载器
        //3.AppClassLoader: 应用类加载器

        // todo 获取类的信息
        Class<Student> studentClass = Student.class;
        //获取类加载器
        System.out.println(studentClass.getClassLoader());//结果:sun.misc.Launcher$AppClassLoader@18b4aac2

        Class<String> stringClass = String.class;
        //获取类加载器
        System.out.println(stringClass.getClassLoader());//结果:null

        //加载Java核心类库> 平台类库>自己类
        ClassLoader classLoader = studentClass.getClassLoader();
        //自己类的上一级
        System.out.println(classLoader.getParent());

    }
}

class  Student{

}
