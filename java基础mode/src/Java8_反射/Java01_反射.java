package Java8_反射;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Java01_反射 {
    public static void main(String[] args) throws NoSuchFieldException, NoSuchMethodException {
        //todo 反射
        User user = new User();
        //类对象
        Class<? extends User> aClass = user.getClass();

        // todo 获取类的名称
        System.out.println(aClass.getName());//获取类的完整名称(包含包名);
        System.out.println(aClass.getSimpleName());//获取类的名称
        System.out.println(aClass.getPackage());//获取类的包的名称

        // todo 获取类的父类
        System.out.println(aClass.getSuperclass());

        // todo 获取类的接口
        Class<?>[] interfaces = aClass.getInterfaces();
        System.out.println(interfaces.length);

        // todo 获取类的属性
        Field field = aClass.getField("***");//public
        Field declaredField = aClass.getDeclaredField("*********");//所有权限

        // todo 获取类的方法
        Method test = aClass.getMethod("test");//public
        Method declaredMethod = aClass.getDeclaredMethod("test");//所有权限

        Method[] methods = aClass.getMethods();//public
        Method[] declaredMethods = aClass.getDeclaredMethods();//所有权限

        // todo 构造方法
        Constructor<? extends User> constructor = aClass.getConstructor();
        Constructor<?>[] constructors = aClass.getConstructors();
        aClass.getDeclaredConstructors();

        // todo 获取权限(修饰符):多个修饰符会融合成一个int值
        int modifiers = aClass.getModifiers();//获取修饰符
        boolean aPrivate = Modifier.isPrivate(modifiers);// 判断是否是这个修饰符


    }
}

class User{
    public void test(){
        System.out.println("1111");
    }
}
