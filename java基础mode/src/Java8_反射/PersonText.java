package Java8_反射;


import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author root
 */

public class PersonText {

    /**
     * 反射前
     */
    @Test
    public void show(){
      Person person =new Person(1,"张三");
      person.age=2;
        System.out.println(person.toString());

    }
    /**
     * 反射后
     */
    @Test
    public void show2() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<Person> clazz=Person.class;
        //获取构造器
        Constructor cons = clazz.getConstructor(int.class, String.class);
        Object o = cons.newInstance(1, "212");
        System.out.println(o.toString());


    }
}
