package 多线程.线程池;

/**
 * 多线程实现方式一：Thread类
 * 1.首先创建一个继承Thread类的子类;
 * 2.重写hread类的方法，逻辑会写在这个方法里面;
 * 3.声明一个Thread子类的对象；
 * 4.调用Thread的start方法启动线程;两个作用：第一启动线程：第二自动执行run方法
 * <p>
 * 例子 ：求100以内的偶数；
 */
class show {
    public static void main(String[] args) {
        ThreadText text = new ThreadText();
        ThreadText text2 = new ThreadText();
        //4.调用Thread的start方法启动线程;
        text.start();
//        for (int i = 0; i < 100; i++) {
//            if (i%2!=0){
//                System.out.println(Thread.currentThread().getName());
//                System.out.println(i);
//            }
//
//        }
        //创建Thread类的匿名子类
        new Thread(){
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    if (i%2!=0){
                        System.out.println(Thread.currentThread().getName());
                        System.out.println(i);
                    }

                }
            }
        }.start();
    }
}

public class ThreadText extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i%2==0){
                System.out.println(Thread.currentThread().getName());
                //设置优先级

                Thread.currentThread().setPriority(1);
                //获取优先级
                System.out.println("优先级："+ Thread.currentThread().getPriority());
                System.out.println(i);
            }

        }
    }
}
