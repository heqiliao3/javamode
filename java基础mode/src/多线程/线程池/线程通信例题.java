package 多线程.线程池;

/**
 * 线程通信:
 *  涉及到的三个方法：
 *     wait():一旦执行此方法，当前线程就进去阻塞状态，并是释放同步监视器;
 *     notify():一旦执行此方法，就会唤醒被wait的一个线程。如果有多个线程被wait,就唤醒优先级高的；
 *     notifyAll():一旦执行此方法，就会唤醒所有被wait的线程；
 *  注意：wait(),notify(),notifyAll()三个方法必须使用在同步代码块或同步方法中;
 *       wait(),notify(),notifyAll()三个方法的调用者必须使用在同步代码块或同步方法中的同步监视器;
 *  面试题：sleep()和wait()的异同？
 *   1.相同点：一旦执行方法，都可以使得当前的线程进入阻塞状态;
 *   2.不同点：1）两个方法声明的位置不同：一个在Thread类中sleep(),一个是object类中声明;
 *            2)调用的要求不同：sleep()可以在任何需要的场景下调用，wait()必须使用在同步代码块或者同步方法中；
 *            3)如果两个方法都使用在同步代码块中，sellp()不会释放锁，wait()会释放锁
 */
class show5 {
    public static void main(String[] args) {
        线程通信例题 x = new 线程通信例题();
        Thread thread = new Thread(x);
        Thread thread2 = new Thread(x);
        thread.setName("线程一");
        thread2.setName("线程二");
        thread.start();
        thread2.start();
    }
}

public class 线程通信例题 implements Runnable {
    private static int p = 100;

    @Override
    public void run() {


        while (true) {
            synchronized (this) {
                notify();
                if (p > 0) {
                    System.out.println(Thread.currentThread().getName() + ":" + p);
                    p--;
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {
                    break;
                }
            }
        }

    }
}
