package 多线程.线程池;

/**
 * 使用Runnable接口做火车票案例
 */
class show4{
    public static void main(String[] args) {
        Runnable火车票案例 r = new Runnable火车票案例();
        Thread thread = new Thread(r);
        Thread thread2 = new Thread(r);
        Thread thread3 = new Thread(r);
        thread.start();
        thread2.start();
        thread3.start();
    }
}

public class Runnable火车票案例 implements Runnable {

    private  static  int r=100;
    @Override
    public  void run() {
        while (true){
            //加入了单例模式的懒汉式  双重检查提升效率
            if(r>0){
                synchronized (this){
                    if(r>0){
                        System.out.println(Thread.currentThread().getName()+"票数:"+r);
                        r--;
                    }
                }
            }


        }
    }
}
