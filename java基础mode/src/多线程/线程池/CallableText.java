package 多线程.线程池;

import 设计模式.Text;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;


/**
 * 创建现成的第三个方式： 实现callable接口
 *   如果理解callable接口的方式创建多线程比实现runnable接口创建多线程方式强大？
 *    1.call()可以有返回值的
 *    2.call（）可以抛异常。被外面的操作捕获，获取异常的信息；
 *    3.callable是支持泛型的
 */
class show34{

    public static void main(String[] args) {
        //3.创建Callable接口实现

        CallableText Text = new CallableText();
        //4.将此callable接口实现类的对象作为futruetask构造器中，创建futruetask的对象
        FutureTask textFutureTask = new FutureTask(Text);
        //5.将futuretark的对象作为参数传递到Thread类的构造器中。创建Thread对象，并调用
        Thread thread = new Thread(textFutureTask);
        thread.start();
        try {
            //6.获取callable中call()的返回值；
            Object o = textFutureTask.get();
            System.out.println(o);
        }catch (InterruptedException e){
            e.printStackTrace();
        }catch (ExecutionException e){
            e.printStackTrace();
        }

    }
        }
//1.创建一个实现Callable的实现类
public class CallableText implements Callable {
    //2.实现call方法，将此线程需要执行的操作声明在call（）中
    @Override
    public Object call() throws Exception {
         int sum=0;
        for (int i = 0; i <100 ; i++) {
             if(i%2==0){

                 System.out.println(Thread.currentThread().getName()+":"+i);
                 sum+=i;

             }
        }
        return sum;
    }
}
