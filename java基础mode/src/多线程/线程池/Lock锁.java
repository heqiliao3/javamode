package 多线程.线程池;

import java.util.concurrent.locks.ReentrantLock;

/** 解锁线程的方式有两种一种用synchronized，另一种就是lock锁
 * Lock锁
 * ReentrantLock 是lock的实现类
 * 面试题：synchronized和lock的异同？
 *   相同：二者都是可以解决线程安全的问题
 *    不同： synchronized 在执行完相应的同步代码之后，他就会自动释放；
 *           lock：是手动加锁和手动释放锁
 */
class show9{
    public static void main(String[] args) {
        Lock锁 锁 = new Lock锁();
        Thread thread = new Thread(锁);
        Thread thread2 = new Thread(锁);
        thread.start();
        thread2.start();
    }
}
public class Lock锁 implements Runnable{
    private static  int p=100;
     //1.new 一个ReentrantLock的对象 ReentrantLock(true)就是交替执行
    private ReentrantLock reentrantLock=new ReentrantLock();
    @Override
    public void run() {

        while (true){
            try {
                //2.利用lock()方法，锁住
                reentrantLock.lock();
                if(p>0){

                    System.out.println(Thread.currentThread().getName()+":"+p);
                    p--;
                }else {
                    break;
                }
            }finally {
                //3.调用解锁方法unlock()；
                reentrantLock.unlock();
            }

        }

    }
}
