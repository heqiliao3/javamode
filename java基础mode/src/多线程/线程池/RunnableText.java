package 多线程.线程池;

/**
 * 创建多线程的方式二：实现runnable接口
 *    1.创建一个实现了runnable接口的类
 *    2.实现类去实现runnable中的抽象方法 run();
 *    3.创建实现类对象
 *    4.将此对象传递到Thread类的构造器中。创建Thread类的对象；
 *    5.Thread对象调用start（）方法；
 */
class show3{
    public static void main(String[] args) {
        //3.创建实现类对象
        RunnableText runnableText = new RunnableText();
        //4.将此对象传递到Thread类的构造器中。创建Thread类的对象；
        Thread thread = new Thread(runnableText);
        Thread thread1 = new Thread(runnableText);
        //5.Thread对象调用start（）方法；
        thread.start();
        thread1.start();
    }

}
//1.创建一个实现了runnable接口的类
public class RunnableText implements Runnable {

    //2.实现类去实现runnable中的抽象方法 run();
    @Override
    public void run() {
        for (int i = 0; i <=100 ; i++) {
            if(i%2==0){
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        }
    }
}
