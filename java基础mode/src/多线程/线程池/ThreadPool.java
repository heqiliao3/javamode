package 多线程.线程池;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 线程池
 * java.util.concurrent.Executors :线程池的工厂类，用来生产线程池；
 * static ExecutorService newFixedThreadPool(int nThreads, ThreadFactory threadFactory)
 *   创建一个线程池，重用固定数量的线程，从共享无界队列中运行，使用提供的ThreadFactory在需要时创建新线程。
 *   参数：
 *      int nThreads 创建线程池中线程的数量；
 *   返回值：
 *      ExecutorService：接口，返回的是ExecutorService接口的实现类对象，我们可以使用ExecutorService接口接收（面向接口编程）
 *      java.util.concurrent.ExecutorService.ExecutorService：就是一个线程池的接口；
 *         用来从线程池中获取线程，调用start方法，执行线程；
 *          submit(Runnable task) 提交一个Runnable任务用于执行
 *         关闭和销毁线程池的方法：
 *              viod shutdown();
 *      线程池的使用步骤：
 *            1.使用线程池的工厂类Executors里面提供了一个静态方法newFixedThreadPool来生产一个指定数量线程数量的线程池；
 *            2.创建一个类，实现Runnable 类，重写run（）方法，设置线程任务
 *            3.调用ExecutorService中的方法submit，传递线程任务（实现类），开启线程，执行run方法；
 *            4.调用ExecutorService中的方法shutdown，关闭线程池（不建议使用）
 *       线程池的四中类型：
 *            Executors.newCachedThreadPool():创建一个可根据需要创建新线程的线程池
 *            Executors.newFixedThreadPool(n):创建一个可重用固定线程数的连接池
 *            Executors.newSingleThreadExecutor():创建一个只有一个线程的线程池；
 *            Executors.newScheduledThreadPool(n):创建一个线程池，它可安排在给定延迟后运行命令或者定期的执行；
 *        属性：
 *            corePoolSize:核心池大小；
 *            maximumPoolSize:最大线程数;
 *            keepAliveTime:线程没有任务的情况下最多保持多长时间会中止；
 */
public class ThreadPool {

    public static void main(String[] args) {
        //1.使用线程池的工厂类Executors里面提供了一个静态方法newFixedThreadPool来生产一个指定数量线程数量的线程池；
        ExecutorService Service = Executors.newFixedThreadPool(5);
        //3.调用ExecutorService中的方法submit，传递线程任务（实现类），开启线程，执行run方法；
        Service.execute(new RunnableText());//适用于runnable
        //Service.submit(new RunnableText());//适用于callable

        Service.shutdown();




    }
}
