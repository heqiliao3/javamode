package 算法;

public class 二分查找法 {
    public static void main(String[] args) {
        // todo 常见类和对象-数组-二分查找法
        //数组
        int[] nums={1,2,3,4,5,6,7};
        // 查找数组中4的下标
        //要查找的数据
        int target =2;
        //开始位置
        int start =0;
        //结束位置
        int end =nums.length-1;
        //中间值
        int middle =0;
        while (start <= end){
            middle = (start + end) / 2;
            if(nums[middle]>nums[target]){
                end=middle-1;
            }if(nums[middle]<nums[target]){
                start=middle+1;
            }else{
                break;
            }

        }
        System.out.println("数据在数组的中的位置:"+middle);
    }
}
