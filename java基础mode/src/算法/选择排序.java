package 算法;

public class 选择排序 {
    public static void main(String[] args) {
         // todo 常见类和对象-数组-选择排序
        /* 选择排序和冒泡排序的差异
            冒泡排序:是前后比较大小，然后交换位置，然后再比较再交换位置;
            选择排序:是先找出数组中最大的位置,然后和最后一个交换位置，性能上要比冒泡排序快;
        */
        //数组
        int[] nums={6,8,2,4,7,9,5,6,1,3};
        for (int j = 0; j < nums.length; j++) {
            int maxIndex=0;
            //找出数组中最大的数
            // nums.length-j 每次循环完最后一个是最大的了，要-1不用循环了
            for (int i = 0; i < nums.length-j; i++) {
                if(nums[i]>nums[maxIndex]){
                    maxIndex=i;
                }

            }
            //最大的数和最后一个数交换位置
            int num1 = nums[nums.length-j-1];
            int num2 = nums[maxIndex];
            nums[maxIndex]=num1;
            nums[nums.length-j-1]=num2;
        }

        //打印遍历
        for (int num:nums) {
            System.out.print(num+" ");
        }

    }
}
