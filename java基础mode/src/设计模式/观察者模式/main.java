package 设计模式.观察者模式;

/**
 * 观察者模式测试
 * @author 白小布
 */
public class main {
    public static void main(String[] args) {
        F_Observer f_observer = new F_Observer();
        S_Observer s_observer = new S_Observer();
        T_Observer t_observer = new T_Observer();
        Theme theme = new Theme();
        /**
         * 订阅
         */
        theme.addAttach(f_observer);
        theme.addAttach(t_observer);
        //theme.addAttach(s_observer);
        /**
         * 修改通知
         */
        theme.setMsg("3");
        s_observer.update("5");
    }

}
