package 设计模式.观察者模式;

import javax.lang.model.SourceVersion;

/**
 * 第二订阅者
 * @author 白小布
 */
public class T_Observer implements Observer {
    @Override
    public void update(String msg) {
        System.out.println( T_Observer.class.getName()+":" + msg);
    }
}
