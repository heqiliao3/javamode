package 设计模式.观察者模式;

/**
 *
 * @author 白小布
 */
public interface Subject {

    /**
     * 新增
     */
    public void add();

    /**
     * 删除
     */
    public void delete();

    /**
     * 修改
     */
    public void update();
}
