package 设计模式.观察者模式;

import java.util.ArrayList;
import java.util.List;

/**
 * 主题
 * @author 白小布
 */
public class Theme {

    /**
     * 订阅者
     */
   private List<Observer>  observers = new ArrayList<Observer>();

    /**
     * 修改
     */

    public void setMsg(String msg){
        notifyAll(msg);
    }
    /**
     * 订阅
     */
    public void addAttach(Observer observer) {
        observers.add(observer);
    }

    /**
     * 通知所有订阅的观察者
     */
    private void notifyAll(String msg){
        for (Observer observer:observers) {
            observer.update(msg);
        }
    }
}
