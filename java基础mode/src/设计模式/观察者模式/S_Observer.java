package 设计模式.观察者模式;

/**
 * 第三订阅者
 * @author 白小布
 */
public class S_Observer  implements Observer{

    @Override
    public void update(String msg) {
        System.out.println(S_Observer.class.getName()+":" + msg);
    }
}
