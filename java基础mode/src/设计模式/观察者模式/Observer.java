package 设计模式.观察者模式;

/**
 * 修改接口
 * @author 白小布
 */
public interface Observer {
    /**
     * 修改
     * @param msg
     */
    public void update(String msg);
}
