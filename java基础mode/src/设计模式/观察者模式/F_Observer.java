package 设计模式.观察者模式;

/**
 *  第一订阅者
 * @author 白小布
 */
public class F_Observer implements Observer{

    @Override
    public void update(String msg) {
        System.out.println(F_Observer.class.getName()+":" + msg);
    }
}
