package 设计模式.工厂模式.简单工厂.entity;

/**
 * 戴尔鼠标
 * @author 白小布
 */
public class Veggie extends Pizza {
    public Veggie(String name) {
        setName(name);
    }

    public void setName(String name) {
        System.out.println(name+"鼠标");
    }
}