package 设计模式.工厂模式.简单工厂.entity;

/**
 * 联想鼠标
 * @author 白小布
 */
public class CheesePizza  extends Pizza{
    public CheesePizza(String name) {
        setName(name);
    }

    public void setName(String name) {
        System.out.println(name+"鼠标");
    }

}
