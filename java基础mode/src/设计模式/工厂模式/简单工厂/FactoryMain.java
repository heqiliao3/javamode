package 设计模式.工厂模式.简单工厂;

import 设计模式.工厂模式.简单工厂.entity.Pizza;

/**
 * 简单工厂测试
 * @author 白小布
 */
public class FactoryMain {

    public static void main(String[] args) {
        // 1. 创建Pizza简单工厂
        SimplePizzaFactory simplePizzaFactory=new SimplePizzaFactory();
        // 2.根据type创建Pizza
        Pizza veggiePizza = simplePizzaFactory.createPizza("1");
        // 3.得到Pizza
        System.out.println("veggiePizza = " + veggiePizza);
    }


}
