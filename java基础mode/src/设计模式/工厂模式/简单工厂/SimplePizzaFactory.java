package 设计模式.工厂模式.简单工厂;

import 设计模式.工厂模式.简单工厂.entity.CheesePizza;
import 设计模式.工厂模式.简单工厂.entity.Pizza;
import 设计模式.工厂模式.简单工厂.entity.Veggie;

/**
 * 工厂模式分为：简单工厂，抽象工厂，工厂方法
 *    （简单工厂分为特例，在设计模式一书中分为工厂方法和抽象工厂）
 *    作用： 生产对象
 *
 *
 *   简单工厂类(鼠标厂)
 */
public class SimplePizzaFactory {

    /**
     * 根据不同pizza类型创建对应Pizza
     * @param type
     * @return
     */
    public Pizza createPizza(String type) {
        if ("1".equals(type)) {
            return new CheesePizza("联想");
        } else if ("2".equals(type)) {
            return new Veggie("惠普");
        } else {
            return null;
        }
    }
}
