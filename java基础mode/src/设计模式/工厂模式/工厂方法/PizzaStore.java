package 设计模式.工厂模式.工厂方法;

import 设计模式.工厂模式.简单工厂.entity.Pizza;

/**
 *  PizzaStore 作为超类，可以被其他
 *  各个加盟店之间的区别在于风味不同，而其它流程我们认为是相同的
 * @author 白小布
 */
public abstract class PizzaStore {


}
