package 设计模式.代理模式.Cglib动态代理;

/**
 * @author root
 */
public class AdminCglibService {
    public void update() {
        System.out.println("修改管理系统数据");
    }

    public Object find() {
        System.out.println("查看管理系统数据");
        return new Object();
    }
}