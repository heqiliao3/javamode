package 设计模式.代理模式.JDK动态代理;

import 设计模式.代理模式.静态代理模式.AdminService;
import 设计模式.代理模式.静态代理模式.AdminServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 *  jdk动态代理：
 *     原理：过滤器和反射
 *     java.lang.reflect.Proxy:生成动态代理类和对象；
 *     java.lang.reflect.InvocationHandler（处理器接口）:可以通过invoke方法实现
 *     a.ClassLoader loader：指定当前target对象使用类加载器，获取加载器的方法是固定的；
 *     b.Class<?>[] interfaces：target对象实现的接口的类型，使用泛型方式确认类型
 *     c.InvocationHandler invocationHandler:事件处理,执行target对象的方法时，会触发事件处理器的方法，会把当前执行target对象的方法作为参数传入。
 */
public class DynamicProxyTest {
    public static void main(String[] args) {

        // 方法一
        System.out.println("============ 方法一 ==============");
        AdminService adminService = new AdminServiceImpl();
        System.out.println("代理的目标对象：" + adminService.getClass());

        AdminServiceInvocation adminServiceInvocation = new AdminServiceInvocation(adminService);

        AdminService proxy = (AdminService) new AdminServiceDynamicProxy(adminService, adminServiceInvocation).getPersonProxy();

        System.out.println("代理对象：" + proxy.getClass());
        int a= 1/0;
        Object obj = proxy.find();
        System.out.println("find 返回对象：" + obj.getClass());
        System.out.println("----------------------------------");
        proxy.update();

        //方法二
        System.out.println("============ 方法二 ==============");
        //1.创建一个AdminServiceImpl对象
        AdminService target = new AdminServiceImpl();
        //2.AdminServiceInvocation实现InvocationHandler接口,传入对象触发invoke()方法,然会返回AdminServiceInvocation对象
        AdminServiceInvocation invocation = new AdminServiceInvocation(adminService);
        //3.通过Proxy.newProxyInstance()获取代理对象
        // TODO: 2021/3/30 不清楚这两个写法的区别
        AdminService proxy2 = (AdminService) Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), invocation);
        AdminService proxy4 = (AdminService) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class[]{AdminService.class}, invocation);
        //4.通过代理对象调用AdminServiceImpl的方法
        Object obj2 = proxy2.find();
        System.out.println("find 返回对象：" + obj2.getClass());
        System.out.println("----------------------------------");
        proxy2.update();

        //方法三
        System.out.println("============ 方法三 ==============");
        final AdminService target3 = new AdminServiceImpl();
        AdminService proxy3 = (AdminService) Proxy.newProxyInstance(target3.getClass().getClassLoader(), target3.getClass().getInterfaces(), new InvocationHandler() {

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("方法执行前——————判断用户是否有权限进行操作");
                Object obj = method.invoke(target3, args);
                System.out.println("方法执行后——————记录用户执行操作的用户信息、更改内容和时间等");
                return obj;
            }
        });

        Object obj3 = proxy3.find();
        System.out.println("find 返回对象：" + obj3.getClass());
        System.out.println("----------------------------------");
        proxy3.update();


    }
}
