package 设计模式.代理模式.JDK动态代理;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class AdminServiceInvocation implements InvocationHandler {

    private Object target;

    public AdminServiceInvocation(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("方法执行前——————判断用户是否有权限进行操作");
        Object invoke = method.invoke(target);
        System.out.println("方法执行后——————记录用户执行操作的用户信息、更改内容和时间等");
        return invoke;
    }
}
