package 设计模式.代理模式.静态代理模式;

import 设计模式.代理模式.静态代理模式.AdminService;

public class AdminServiceImpl implements AdminService {
    public void update() {
        System.out.println("修改管理系统数据");
    }

    public Object find() {
        System.out.println("查看管理系统数据");
        return new Object();
    }
}