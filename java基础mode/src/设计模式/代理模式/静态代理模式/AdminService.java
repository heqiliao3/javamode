package 设计模式.代理模式.静态代理模式;

public interface AdminService {
    void update();
    Object find();
}