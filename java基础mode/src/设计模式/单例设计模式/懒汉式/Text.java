package 设计模式.单例设计模式.懒汉式;



public class Text {
    public static void main(String[] args) {
       /* //懒汉式线程不安全
        Singleton01 ingleton=Singleton01.getIngleton01();
        Singleton01 ingleton2=Singleton01.getIngleton01();
        System.out.println(ingleton == ingleton2);*/
       //枚举
        Singleton05 ingleton=Singleton05.INGLETON;
        Singleton05 ingleton2=Singleton05.INGLETON;
        ingleton.sayok();
        System.out.println(ingleton == ingleton2);
    }

}
