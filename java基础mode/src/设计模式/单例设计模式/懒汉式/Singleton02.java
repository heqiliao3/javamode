package 设计模式.单例设计模式.懒汉式;

/**
 *懒汉式（线程安全，同步方法）：优点：加入了synchronized同步方法，线程安全，
 *                            缺点：效率低下
 */
public class Singleton02 {
    //1.首先在类内部创建一个对象；
    private static  Singleton02 ingleton;
    //2.私有哈构造方法 ，防止外面new 实例化
    private Singleton02() {

    }
    //3.提供一个静态的公共方法，当使用到该方式时：才去创建ingleton 对象;
    public  synchronized static Singleton02 getIngleton02(){
        //判断是否用过
        if(ingleton==null){
            ingleton=new Singleton02();
        }
        return ingleton;
    }
}
