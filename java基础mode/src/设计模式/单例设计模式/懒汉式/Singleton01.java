package 设计模式.单例设计模式.懒汉式;

/**
 * 懒汉式（线程不安全的）：缺点：线程不安全，当多个线程同时经过if(ingleton==null)时，就会编程有多个实例对象；单机程序可以使用
 */
public class Singleton01 {
    //1.首先在类内部创建一个对象；
   private static  Singleton01 ingleton;
   //2.私有哈构造方法 ，防止外面new 实例化
    private Singleton01() {

    }
    //3.提供一个静态的公共方法，当使用到该方式时：才去创建ingleton 对象;
    public static Singleton01 getIngleton01(){
        //判断是否用过
        if(ingleton==null){
            ingleton=new Singleton01();
        }
        return ingleton;
    }
}
