package 设计模式.单例设计模式.懒汉式;

/**
 * 通过枚举方式，实现单例模式
 */
 enum  Singleton05 {
    INGLETON;

    public  void sayok(){
        System.out.println("ok");
    }
}
