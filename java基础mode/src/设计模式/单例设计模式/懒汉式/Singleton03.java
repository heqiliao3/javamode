package 设计模式.单例设计模式.懒汉式;

/**
 *懒汉式（线程安全，同步方法） + 双重检查代码   平时工作中推荐使用
 *     解决了线程安全问题，又解决了懒加载的问题；
 */
public class Singleton03 {

    //1.首先在类内部创建一个对象；
    private static  volatile  Singleton03 ingleton;
    //2.私有哈构造方法 ，防止外面new 实例化
    private Singleton03() {

    }
    //3.提供一个静态的公共方法，当使用到该方式时：才去创建ingleton 对象;
    public   static Singleton03 getIngleton03(){
        //第一个不等于null时判断，多线程情况下有个线程进去了没有执行完，另一个线程也会进去;
        if(ingleton==null){
            //如果第二个线程进来了，就要在这里等待
            synchronized (Singleton03.class){
                //第二个线程进来发现，第一个线程已经实例化了（ingleton有值了），就不会在实例化对象了
                if(ingleton==null){
                    ingleton=new Singleton03();
                }

            }
        }
        return ingleton;
    }
}
