package 设计模式.单例设计模式.懒汉式;

/**
 * 静态内部类  延迟加载，效率高  推荐使用
 *    1.当类被装载的时候,不会装载静态内部类;只有在调用的时候才会被装载，去实例化对象
 *     2.静态内部类只能被执行一次，所以就没有线程安全的问题
 *     3.类的静态属性只会在第一次加载类的时候初始化，
 */

public class Singleton04 {

    //1.私有哈构造方法 ，防止外面new 实例化
    private Singleton04() {

    }
    //2.静态内部类
    public  static class SingletonInstance{
        private static  volatile  Singleton04 ingleton=new Singleton04();
    }
     //3.调用静态内部类，实例化对象   jvm装载类的时候，线程时安全，这里利用了jvm类的装载机制实现线程安全
    public   static Singleton04 getIngleton03(){

        return SingletonInstance.ingleton;
    }
}
