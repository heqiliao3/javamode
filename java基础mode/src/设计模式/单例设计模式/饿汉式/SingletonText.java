package 设计模式.单例设计模式.饿汉式;

/**
 * 设计模式.单例设计模式
 *   饿汉式（静态变量）
 *   饿汉式（静态代码块） ：优缺点一样  优点：避免了线程同步问题；缺点：可能造成内存浪费，
 *   因为不管你用不用我都实例化对象；没有按需加载
 */
public class SingletonText {
    public static void main(String[] args) {
        Singleton singleton=Singleton.getInstance();
        Singleton singleton2=Singleton.getInstance();
        System.out.println(singleton == singleton2);
    }
}

/**
 * 饿汉式（静态变量）
 */
class Singleton{
    //1.私有化构造方法，方式外部new 这个类的实例
    private  Singleton(){

    }
    //2.本类内部创建对象实例
    private  final static Singleton singleton=new Singleton();

    //3.提供一个共有的静态方法，返回；
    public static  Singleton getInstance(){

        return singleton;
    }



}
/**
 * 饿汉式（静态代码块）
 */
class Singleton2{
    //1.私有化构造方法，方式外部new 这个类的实例
    private  Singleton2(){

    }
    //2.本类内部创建对象实例
    private   static Singleton2 singleton2;
     //把初始化对象放在静态代码块中执行，优缺点和一样
    static{
        singleton2=new Singleton2();
    }
    //3.提供一个共有的静态方法，返回；
    public static  Singleton2 getInstance2(){

        return singleton2;
    }



}