package Java2_运算符;

/**
 * @author root
 */
public class 关系运算符 {

    public static void main(String[] args) {
        //TODO 关系运算符：所谓的关系运算符其实就是用于比较两个数据之间关系的运算符  == != < > <= >=
        //关系运算符的结果为布尔类型，如果表达式结果为预想相同，返回true,不相同返回false;
        int i=10;
        int j=20;
        System.out.println(i>j);
        System.out.println(i<j);
        System.out.println(i==j);
        System.out.println(i<=j);
        System.out.println(i>=j);
        System.out.println(i!=j);
    }
}
