package Java2_运算符;

public class 逻辑运算符 {
    public static void main(String[] args) {
        // TODO 逻辑运算符:所谓的逻辑运算符其实就是用于描述多个条件表达式之间的关系的运算符;
        // TODO 语法结构  变量=（条件表达式1) 逻辑运算符 （条件表达式2）
        // 结果的变量的数据类型依然为布尔类型
        // TODO 逻辑运算符 : & 称之为与运算
        // 与运算：要求两个表达式都计算出结果，只有当两个结果都是true时，最终结果为true,其他都为false;
        int i =10;
        boolean bln =(i>20)&(i<20);
        System.out.println("& 称之为与运算");
        System.out.println(bln);
        // TODO 逻辑运算符 : | 称为或运算
        // 或运算：要求两个表达式都计算出结果，只有任何一个结果为ture，最终结果为true;如果两个结果都为false,才为false;
        int l =10;
        boolean bl =(l>20)|(l<20);
        System.out.println("| 称为或运算");
        System.out.println(bl);
        // TODO 逻辑运算符 : && 称之为短路与运算
        // 短路与运算: 会根据第一个表达式的结果来判断,是否执行第二个表达式；
        // 如果第一个表达式的结果为false,那么就无需执行第二个表达式
        int o=20,j=20;
        boolean is =(0>20) && (++j<20);
        System.out.println("&& 短路与运算");
        System.out.println(is);
        System.out.println(j);
        // TODO 逻辑运算符 : || 称之为短路或运算
        // 短路或运算: 会根据第一个表达式的结果来判断,是否执行第二个表达式；
        // 如果第一个表达式的结果为false,那么就无需执行第二个表达式
        int o1=20,j1=20;
        boolean is1 =(o1==20) || (++j1<20);
        System.out.println("|| 为短路或运算");
        System.out.println(is1);
        System.out.println(j1);
        // TODO 逻辑运算符 : ! 称之为非(相反)运算
        //true-->false;
        //false-->true;
        int a=10;
        boolean ba=a==10;
        System.out.println("! 称之为非(相反)运算");
        System.out.println(ba);
        System.out.println(!ba);
        // TODO ^ 按位异或运算符(^)是二元运算符
        //按位异或运算符(^)是二元运算符，要化为二进制才能进行计算，在两个操作数中，如果两个相应的位相同，则运算结果为0，否则1
        int a3=15;
        int b=a3^8;
        System.out.println("^ 按位异或运算符(^)是二元运算符");
        System.out.println(a3>b);
        //运算结果为：a=15；b=7；
        //a（二进制）=1111；8（二进制）=1000；1111^1000=0111;er 0111=7(十进制）；
       // 在两个布尔变量里if（boolean a ^ boolean b）就是 当a和b同真或同假时为假，反之为真。
    }
}
