package Java2_运算符;

/**
 * 赋值运算符
 * = += -= *= /= %=
 *
 */
public class 赋值运算符 {
    public static void main(String[] args) {
        //TODO 赋值运算符
        //等号其实就是赋值运算符，把等号右边的数据赋值到等号左边;
        String name="zhangsan";
        // TODO 复核赋值运算符: += -= *= /= %=
        int i=2;
        i+=2;
        System.out.println(i);
        i*=2;
        System.out.println(i);
        i/=2;
        System.out.println(i);
        i%=3;
        System.out.println(i);
        i-=1;
        System.out.println(i);
        //如果用了赋值运算符，那么数据的类型不会发生改变；
        byte e=10;
        //e=e+20; //int -->byte,会报错;
        e+=20;  //那么用了赋值运算符就不会报错;
        System.out.println(e);


    }
}
