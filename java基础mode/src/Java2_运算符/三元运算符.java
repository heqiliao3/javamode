package Java2_运算符;

public class 三元运算符 {
    public static void main(String[] args) {
        // todo 三元运算符: 所谓的三元运算符其实就是三个元素参与运算；
        //结构: 变量=(条件表达式)？（任意表达式1）（任意表达式2）；
        // 运算规则：判断条件表达式为true,执行表达式1，为false,执行表达式2；
        int i=10;
        int j=20;
        int k=(i>j)? i:j;
        System.out.println(k);
    }
}
