package Java2_运算符;
public class 算术运算符 {
    public static void main(String[] args) {

        // TODO 运算符
        //运算符就是参与数据运算的符号;这个是java定义的，无法自行定义;
        // TODO 表达式
        // 所谓的表达式就是采用运算符和数据连接在一起形成符合java语法规则的指令代码,称之为表达式
        // TODO 算术运算符
        //1.一元运算符:一个元素参与运算的运算符; ++ --;
// TODO i++
//        int i = 0;
//        int  j = i;
//        i=i+1;
//        System.out.println("i: "+i);
//        System.out.println("j: "+j);
//         int i = 0;
//         int  j = i++;
//         System.out.println("i: "+i);
//         System.out.println("j: "+j);

//        int i = 0;
//        i=1+i;
//        int  j = i;
//        System.out.println("i: "+i);
//        System.out.println("j: "+j);

// TODO ++i
//        int i = 0;
//        i=1+i;
//        int  j = i;
//        System.out.println("i: "+i);
//        System.out.println("j: "+j);

//            int i = 0;
//            int  j = ++i;
//            System.out.println("i: "+i);
//            System.out.println("j: "+j);
// TODO i--
//            int i = 1;
//            int  j = i;
//            i=i-1;
//            System.out.println("i: "+i);
//            System.out.println("j: "+j);

//         int i = 1;
//         int  j = i--;
//         System.out.println("i: "+i);
//         System.out.println("j: "+j);
// TODO --i
//            int i = 1;
//             i=i-1;
//            int  j = i;
//            System.out.println("i: "+i);
//            System.out.println("j: "+j);

//         int i = 1;
//         int  j = --i;
//         System.out.println("i: "+i);
//         System.out.println("j: "+j);

        // 2.二元运算符:两个元素参与运算的运算符 1+2; + - * / %
        // TODO 算术表达式=元素1  二元运算符  元素2
        //运算结果取的是类型最大的那一种，最小使用类型为int;
        //加
        System.out.println(1+2);
        //减
        System.out.println(2-2);
        //乘
        System.out.println(3*2);
        //除
        System.out.println(4/2);
        //int int -->int 所以就是0;
        System.out.println(1/2);
        //double int -->double 所以就是0.5;
        System.out.println(1.0/2);
        //取余 ,模运算
        System.out.println(5/2);

        //最小使用数据为int;
        byte a=10;
        byte b=20;
        int c=a+b;
        System.out.println(c);
    }
}
