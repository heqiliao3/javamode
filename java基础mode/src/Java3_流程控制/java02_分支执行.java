package Java3_流程控制;

public class java02_分支执行 {
    public static void main(String[] args) {
        // todo 分支执行
        // 1. todo 可选分支：单分支结构
        System.out.println("单分支结构");
        System.out.println("第一步");
        System.out.println("第二步");
        int i=10;
        if (i==20){
            System.out.println("分支");

        }
        System.out.println("第三步");
        // 2. todo 可选分支：双分支结构（二选一）
        System.out.println("双分支结构");
        System.out.println("第一步");
        System.out.println("第二步");
        int j=10;
        if (j==20){
            System.out.println("分支1");

        }else {
            System.out.println("分支2");
        }
        System.out.println("第三步");
        // 3. todo :多分支:多重判断
        //if-->else if-->[else]
        System.out.println("多分支");
        System.out.println("第一步");
        System.out.println("第二步");
        int k=10;
        if (k==20) {
            System.out.println("分支1");
        }else if(k==10){
            System.out.println("分支2");
        }else {
            System.out.println("分支3");
        }
        System.out.println("第三步");
        // 4. todo 特殊分分支结构
        //switch(){}
        //switch语法会对数据进行判断,如果等于某一个分支数据的值，那么执行对应分支的逻辑代码
        //如果一个分支都无法满足，那么一个分支都不会执行，如果想无法匹配也需要执行分支，那么可以增加default关键字；
        //如果执行某一个分支后，不想继续执行其他分支，那么可以使用break关键字，跳出分支结构；
        System.out.println("特殊分分支结构");
        System.out.println("第一步");
        System.out.println("第二步");
        int a=10;
        switch (a){
            case 10:
                System.out.println("分支1");
                break;
            case 20:
                System.out.println("分支2");
            case 30:
                System.out.println("分支3");
            default:
                System.out.println("分支4");
        }
        System.out.println("第三步");
    }

}
