package Java3_流程控制;

/**
 *  java中主要用于循环的语句有3个：
 *    1.while :有条件循环
 *    2.do...while :
 *    3.for
 */
public class java03_重复执行 {
    public static void main(String[] args) {
        //todo 流程控制-重复执行（循环执行）
       //java中主要用于循环的语句有3个：
       //1.while :有条件循环
        /*
        基本语法：while(条件表达式1){
            需要循环的代码
        }
        */
       // 执行原理： while循环会判断条件表达式的结果是否为true,如果为false则跳出循环，如果为true,会执行大括号内的逻辑代码，则重新判断条件表达式执行循环代码;
        int i=20;
        while (i<30){
            System.out.println("循环代码");
            i++;
        }
     //2.do...while
        /*
         基本语法；
          do{
            循环代码
          }while();
         */
        //执行原理：和while循环语法基本相同，区别就在于循环代码是否至少执行一次：
        int j=10;
        do {
            System.out.println("循环代码");
        }while(j>10);
       //3.for
       /* for (int k = 0; k < args.length; k++) {
           循环代码；
        }
        */

        int k=10;
        System.out.println("for循环");
        for (int l = 0; l < k; l++) {
            System.out.println("循环代码"+l);
        }

        // break :直接跳出循环，不再执行后续操作；

        System.out.println("break");
        for (int l = 0; l < k; l++) {
            if(l==4){
                break;
            }
            System.out.println(l);
        }
        // continue :直接跳出当前循环，执行后续循环操作；
        System.out.println("continue");
        for (int l = 0; l < k; l++) {
            if(l==4){
                continue;
            }
            System.out.println(l);
        }
    }
}
