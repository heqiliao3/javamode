package Java3_流程控制;

/**
 * @author root
 */
public class 循环练习_九层妖塔 {
    public static void main(String[] args) {
        // todo 循环练习-九层妖塔
        /*
                *
               ***
              *****
             *******
         */
        // println方法打印字符串后，会自动添加换行符；
        // print方法打印字符串后，不换行；
        int le=9;
        for (int j = 0; j < le ; j++) {
            for (int i = 0; i < (le-1)-j; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < j*2+1; i++) {
                System.out.print("*");
            }
            System.out.println(" ");
        }

    }
}
