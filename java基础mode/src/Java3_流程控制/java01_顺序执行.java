package Java3_流程控制;

/**
 * @author root
 *
 * java的流程控制主要分为三种：
 *   1.顺序执行；
 *   2.分支执行；
 *   3.重复执行;
 */
public class java01_顺序执行 {
    public static void main(String[] args) {
        // todo 流程控制
        // 所谓的流程控制:其实就是计算机在执行代码时，对指令代码执行顺序的控制；
        //1.顺序执行
        //变量在使用之前，需要声明并初始化;
        int i =10;
        int j =20;
        System.out.println(i+j);

    }
}
