package Java5_常用类和对象;

public class Java11_字符串_替换 {
    public static void main(String[] args) {
        // todo 常见类和对象-字符串-替换
        // todo replace方法用于字符串的替换
        String a ="hello world zhangsan";
        //replace 替换
        System.out.println(a.replace("world", "java"));
        //replaceAll 按照指定规则替换 world|zhangsan 意思就是world或者zhangsan都要替换
        System.out.println(a.replaceAll("world|zhangsan", "java"));

    }
}
