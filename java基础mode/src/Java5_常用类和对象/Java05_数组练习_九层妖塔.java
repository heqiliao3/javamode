package Java5_常用类和对象;

public class Java05_数组练习_九层妖塔 {
    public static void main(String[] args) {
        // todo 常见类和对象-数组练习-九层妖塔
        /*
              *
             ***
            *****
         */
        int row=9;
        int column = 2 * (row-1)+1;
        String[][] nineTower=new String[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if(j>=(row-1)-i && j<=(row-1)+i){
                    System.out.print("*");
                }else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
