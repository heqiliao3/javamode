package Java5_常用类和对象;

public class Java13_字符串_查找 {
    public static void main(String[] args){
        // todo 常见类和对象-字符串-查找
        String s="hello hello world world";
        //查找字符
        char[] chars = s.toCharArray();
        //查找字节
        byte[] bytes = s.getBytes();
        //charAt() 可以传递索引定位字符串中指定的字符
        System.out.println(s.charAt(1));
        //indexOf 方法用于获取数据在字符串中第一次出现的位置
        System.out.println(s.indexOf("hello"));
        //lastIndexOf 方法用于获取数据在字符串中最后一次出现的位置
        System.out.println(s.lastIndexOf("hello"));
        // 是否包含指定的字符串，返回布尔类型
        System.out.println(s.contains("hello"));
        //判断字符串是否以指定的数据开头和结尾，返回布尔类型
        System.out.println(s.startsWith("hello"));
        System.out.println(s.endsWith("hello"));
        //判断字符串是否为空，空格其实是一个特殊的字符，所以看不到，但是不为空
        System.out.println(s.isEmpty());
    }
}
