package Java5_常用类和对象;

public class Java18_比较 {
    public static void main(String[] args) {
        // todo 常见类和对象-比较
        // 基本数据类型，双等号比较数值
        int i = 10;
        int j =10;
        System.out.println(j==i);// 结果 true
        double d=10.00;
        System.out.println(d==j);// 结果 true
        // todo 一般基本数据类型我们用==判断。引用数据类型我们用equals判断
        //引用数据类型，双等号比较变量的内存地址是否相等
        String s ="asd";
        String f="asd";
        //s==f 是true是因为String s ="asd";java虚拟机把它认定为常量，保存在字符串常量池中，所以他们的内存地址一样；
        System.out.println(s==f);// 结果 true
        String s1 = new String("asd");
        //f==s1结果false，是因为s1是对象，它保存在堆里面，f是常量，保存在常量池中，他们的内存地址不一样
        System.out.println(f==s1);// 结果false
        //equals比较的是内容是否相等
        System.out.println(s1.equals(f));// 结果 true

        User9 user9 = new User9();
        User user = new User();
        //两个对象对比他是用的Object的equals方法，比较的是内存地址是否相等，user9和user它们是new的两个新对象保存在堆里面，所以他们的内存地址不相等;
        System.out.println(user9.equals(user));//结果false
        //包装类型:也是引用数据类型

//        Integer i1=100;
//        Integer i2=100;
//        System.out.println(i1==i2);
        //Integer缓存:-128~127,不超过这个范围相等，超过这个返回不相等
        Integer i1=Integer.valueOf(128);
        Integer i2=Integer.valueOf(128);
        System.out.println(i1==i2);
        //引用数据类型不要用==。用equals
        System.out.println(i1.equals(i2));

    }
}

class User9{
    @Override
    public int hashCode() {
        return 1;
    }

    //想要让User9类new出的对象都相等，就可以和String类一样重写equals方法，比较内容是否相等；
    @Override
    public boolean equals(Object obj) {
        return true;
    }
}
