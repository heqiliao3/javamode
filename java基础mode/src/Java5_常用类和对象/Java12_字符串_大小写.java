package Java5_常用类和对象;

public class Java12_字符串_大小写 {
    public static void main(String[] args) {
        // todo 常见类和对象-字符串-大小写
        //   toLowerCase()转换小写
        //   toUpperCase()转换大写
        String s ="user";
        String d ="USER";
        System.out.println(d.toLowerCase());
        System.out.println(s.toUpperCase());

        // 让user驼峰显示
        String substring = s.substring(0, 1);
        String substring1 = s.substring(1);
        System.out.println(substring.toUpperCase()+substring1);
    }
}
