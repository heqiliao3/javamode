package Java5_常用类和对象;

public class Java08_字符串 {
    public static void main(String[] args) throws Exception {
        // todo 常见类和对象-字符串
        // todo 字符串连续字符组成形成的数据类型
        // java.lang.String
        // 字符串，字符，字节的关系
        //ASCII编码是单字节编码，只有英文字符，不能编码汉字。
       // GBK编码1个英文字符是1个字节，一个汉字是是2个字节。
       // UTF-8编码1个英文字符是1个字节，一个汉字是3个字节。
        //字节
        byte[] b={-28,-72,-83,-27,-101,-67};
        String name = new String(b, "UTF8");
        System.out.println(name);
        System.out.println("**********************************");
        //字符
        char[] c={'a','中','国'};
        String name2 = new String(c);
        System.out.println(name2);
        //转义字符:\" -> 文字中的双引号
        String s="\"";
        System.out.println(s);
        // |' 单引号
        // \t tab键
        // \n 换行
        // \\ \本身
        System.out.println("\'");
        System.out.println("a\tb");
        System.out.println("c\nd");
        System.out.println("\\");
    }
}
