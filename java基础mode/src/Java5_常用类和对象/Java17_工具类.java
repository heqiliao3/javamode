package Java5_常用类和对象;

import java.util.UUID;

public class Java17_工具类 {
    public static void main(String[] args) {
        // todo 常见类和对象-工具类
        System.out.println(StringUtils.isEqual(""));
        System.out.println(StringUtils.isEqual(null));
        System.out.println(StringUtils.isEqual("  "));
        System.out.println(StringUtils.isEqual(" aa "));
        System.out.println(StringUtils.makeString());
    }
}
// 字符串工具类
// 1.工具类不应该创建对象才能使用，也就意味着，可以直接使用类中的属性和方法，一般声明为静态的
// 2.工具类对外提供的属性和方法都应该是公共的
// 3.为了使用者开发方便，应该尽量提供丰富的方法和属性
class StringUtils {
   //非空判断
    public static boolean isEqual(String s1) {
        if (s1 ==null|| "".equals(s1.trim())) return true;
        return false;
    }
    public static boolean isNotEqual(String s1) {
        return !isEqual(s1);
    }
    //生成随机数
    public static String makeString() {
        return UUID.randomUUID().toString();
    }
}
