package Java5_常用类和对象;

public class Java10_字符串_截断 {
    public static void main(String[] args) {
        // todo 常见类和对象-字符串-截取
        String s="  hello world   ";
        //  todo subString 方法用于截取字符串，需要传递两个参数
        //      第一个参数表示截取字符串的起始位置(索引，包含)
        //     第二个参数表示截取字符串的结束位置(索引，不包含)
        System.out.println("subString 方法用于截取字符串");
        System.out.println(s.substring(0,3));
        System.out.println(s.substring(0,"hello".length()));
        System.out.println(s.substring(6,s.length()));
        //subString方法如果只传递一个参数，那么就表示从指定位置开始截取字符串，然后截取到最后
        System.out.println(s.substring(6));

        // todo split方法分解字符串，根据指定的字符串进行分解，可以将一个完整的字符串，分解成多个部分;
        System.out.println("split方法分解字符串");
        String[] splits = s.split(" ");
        System.out.println(splits.length);
        for (String split:splits){
            System.out.println(split);
        }

        // todo trim :去掉字符串的首尾空格的意思
        System.out.println("trim去掉字符串的首尾空格的意思");
        System.out.println("I"+s.trim()+"i");
    }
}
