package Java5_常用类和对象;

public class Java04_数组_冒泡排序 {
    public static void main(String[] args) {
        // todo 常见类和对象-数组-冒泡排序
        //数组
        int[] nums={6,8,2,4,7,9,5,6,1,3};
        //冒泡排序算法 从小到大
        // todo 希望得到 1.2.3.4.5.6.7.8.9
        // 结果 6 2 4 7 8 5 6 1 3 9
        // 它只是把最大的数移到了最后，但是没有整体排序
        for (int i = 0; i <nums.length-1 ; i++) {
            //获取数组中的值
            int num1 = nums[i];
            int num2 = nums[i+1];
            //比较大小
             if(num1 > num2){
                 //如果num1>num2就交换他们的位置
                 nums[i+1]= num1;
                 nums[i]= num2;
             }
        }
        for (int num:nums) {
            System.out.print(num+" ");
        }
        System.out.println("***************************************");
        //j循环把最大的数放到最后，nums.length-l-缩小数组，就可以排序
        //6 2 4 7 8 5 6 1 3 9
        //6 2 4 7 5 6 1 3 8
        for (int l = 0; l <nums.length-1 ; l++) {
            //遍历整个数组把最大的数移到最后
            for (int j = 0; j <nums.length-l-1 ; j++) {
                //获取数组中的值
                int num1 = nums[j];
                int num2 = nums[j+1];
                //比较大小
                if(num1 > num2){
                    //如果num1>num2就交换他们的位置
                    nums[j+1]= num1;
                    nums[j]= num2;
                }
            }
        }

        for (int num:nums) {
            System.out.print(num+" ");
        }
    }

}
