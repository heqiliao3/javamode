package Java5_常用类和对象;

public class Java09_字符串_比较 {
    public static void main(String[] args) {
        // todo 常见类和对象-字符串-比较
        String a="a";
        String b="A";
        System.out.println(a.equals(b));
        //忽略大小写的相等
        System.out.println(a.equalsIgnoreCase(b));
        //比较大小
        //a>b 正数
        //a<b 负数
        //a=b 0
        int i = a.compareTo(b);
        System.out.println(i);
        //比较大小 忽略大小写
        System.out.println(a.compareToIgnoreCase(b));


    }
}
