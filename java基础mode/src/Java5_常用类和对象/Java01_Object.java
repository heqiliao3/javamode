package Java5_常用类和对象;

public class Java01_Object {
    public static void main(String[] args) {
        // todo 常用类和对象-java.lang.Object
        Object obj = new Person();
        // 将对象转换为字符串:
        //toString 默认打印的就是对象的内存地址，所以，为了能够更直观理解对象的内容，所以可以重写这个方法;
        String s = obj.toString();
        System.out.println(s);
        // todo 获取对象的内存地址
        int i = obj.hashCode();
        System.out.println(i);
        // todo  equals()比较对象是否相等:默认时比较内存地址是否相等
        System.out.println(obj.equals(new Person()));
        //getClass():获取类的当前信息
        Class<?> aClass = obj.getClass();
        System.out.println(aClass.getSimpleName());//类
        System.out.println(aClass.getPackage());//包

    }


}

class Person extends Object {
    public String name="张三";

//    @Override
//    public String toString() {
//        return "Person["+name+"]";
//    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}

class User  extends Person{

}
