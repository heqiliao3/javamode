package Java5_常用类和对象;

public class Java15_包装类 {
    public static void main(String[] args) {
        // todo 常见类和对象-包装类
        // int->Integer
        // char->Character
        // long->Long
        // double->Double
        // float->Float
        // double->Double
        // boolean->Boolean
        // short->Short
        int i=10;
        // todo 自动装箱
        Integer a=i;
        // todo 自动拆箱
        int j=a;
    }
}
