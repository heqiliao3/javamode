package Java5_常用类和对象;

public class Java02_数组 {

    public static void main(String[] args) {
        // todo 常见类和对象-数组
        // 数组的声明: 类型[] 变量;
        // 数组的创建: new 类型[容量];
        String[] names =new String[3];
        // 数组是没有数据类型，需要设定数据类型
        // 给数组的小格子添加数据，添加的方式为:数组的变量[索引]=数据;
        // 添加数据和访问数据时,索引是不能超过指定的范围的(0~length-1);
        names[0] ="张三";
        names[0]="麻子";
        names[1]="李四";
        names[2]="王二";
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        Java02 java01 = new Java02();
        java01.test();
        Java02 java02 = new Java02();
        java02.test();
        Java02 java03 = new Java02();
        java03.test();
        System.out.println("********************************************");
        Java02[] java02s = new Java02[3];
        for (int i = 0; i < java02s.length; i++) {
            java02s[i]=new Java02();
            java02s[i].test();
        }

    }



}


class Java02{

        public void test(){
            System.out.println("test....");
        }
}