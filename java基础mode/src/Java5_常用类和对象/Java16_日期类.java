package Java5_常用类和对象;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Java16_日期类 {
    public static void main(String[] args) throws ParseException {
        // todo 常见类和对象-日期类
        // 时间戳
        System.out.println(System.currentTimeMillis());//结果：1675041984740 看不懂
        // Date :日期类
        Date date = new Date();
        System.out.println(date);//结果:Mon Jan 30 09:28:01 CST 2023 还是看不懂
        //java格式化日期格式
        //y(Y) -> 年 -> yyyy
        //m(M) -> MM -> 月份 : mm ->分钟
        //d(D) -> dd -> 一个月中的日期 : D -> 一年中的日期
        //h(H) -> h -> 12进制: HH -> 24进制
        //s(S) -> s -> 秒: S:毫秒
        //一般使用: yyyy-MM-dd HH:mm:ss
        // todo :一个日期转换成字符串 Date -> String
        System.out.println("********************************yyyy-MM-dd HH:mm:ss*********************");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date);
        System.out.println(format);
        //yyyy-MM-dd HH:mm:ss加毫秒
        System.out.println("***********************yyyy-MM-dd HH:mm:ss加毫秒**************************");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String format1 = simpleDateFormat2.format(date);
        System.out.println(format1);
        // todo String -> Date
        System.out.println("************************String -> Date******************************");
        String dateString ="2022-06-01 08:20:50";
        Date parse = simpleDateFormat.parse(dateString);
        System.out.println(parse);
        //根据时间戳构建指定的日期对象
        date.setTime(System.currentTimeMillis());
        //获取时间戳
        date.getTime();
        // 日期比大小
        //判断date < parse
        System.out.println(date.before(parse));
        //判断date > parse
        System.out.println(date.after(parse));
        //date当前时间获取年月日的方法已经不推荐使用了，现在使用日历类Calendar
        Calendar cal = Calendar.getInstance();
        System.out.println(cal);//结果看不懂，所以需要调用它的方法获取
        System.out.println(cal.get(Calendar.YEAR));//获取当前时间的年  结果:2023
        System.out.println(cal.get(Calendar.MONTH));//获取当前时间的月  结果:0 Calendar的月份是从0开始的，0就代表一月
        System.out.println(cal.get(Calendar.DATE));//获取当前时间的日期  结果:30
        //插入日期
        cal.setTime(date);
        // todo 增加日期
        //当前日期加一年
        cal.add(Calendar.YEAR,1);


    }
}
