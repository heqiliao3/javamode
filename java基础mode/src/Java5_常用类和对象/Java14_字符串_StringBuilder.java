package Java5_常用类和对象;

public class Java14_字符串_StringBuilder {
    public static void main(String[] args) {
        // todo 常见类和对象-字符串-StringBuilder
        //因为String是被final修饰的类，所以在这样频繁的拼接对象的情况下，会创建很多对象，浪费资源
        String a =" ";
        for (int i = 0; i < 100; i++) {
            a=a+i;
        }
        System.out.println(a);
        // StringBuilder 在字符串拼接不会创建新的对象，它的底层是有数组实现的
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 100; i++) {
            stringBuffer.append(i);
        }
        System.out.println(stringBuffer);
        System.out.println(stringBuffer.reverse());//反转
        System.out.println(stringBuffer.insert(1, "aaa"));//插入
    }
}
