package Java5_常用类和对象;

public class Java03_二维数组 {
    public static void main(String[] args) {
        // todo 常见类和对象-二维数组
        //一维数组
        System.out.println("**************一维数组************************");
        String[] names= {"张三","李四","王五"};
        System.out.println(names[0]);
        //二维数组
        System.out.println("***********二维数组******************");
        String[][] names2= {{"张三","李四","王五"},{"张三","李四"},{"张三"}};
        System.out.println(names2[0][1]);
        //标准的二维数组
        System.out.println("***************标准的二维数组********************");
        String[][] names3= new String[3][3];
        names3[0][1]="张三";
       // System.out.println(names3[0][1]);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(names3[i][j]+"   ");
            }
            System.out.println();
        }


    }
}
