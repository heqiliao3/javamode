package IO流;

import java.io.*;

/**
 * 字节流的用法：
 *   FileInputStream:普通的字节输入流，用来读取数据的；
 *      构造方法：
 *        public FileInputStream(String pathname)
 *      成员方法：
 *        public int read():一次读取一个字节，并返回读取到的内容，读不到的返回-1;
 *    FileOutputStream:普通的字节输出流，用来写数据的；
 *       构造方法：
 *         public FileOutputStream(String pathname)
 *       成员方法：
 *         public void write(int len): 一次写入一个字节
 * 字节缓冲流用法：
 *  *     分类：
 *  *       BufferedInputStream:字节缓冲输入流
 *  *         构造方法：
 *  *           public BufferedInputStream(InputStream is)
 *  *
 *  *       BufferedOutputStream:字节缓冲输出流
 *  *          构造方法：
 *  *            public BufferedOutputStream(OutputStream os)
 *  *
 *  *      特点：
 *  *          字节缓冲流自带有缓冲区，大小为8192个字符，也就是8kb;
 *    总结 ：
 *       拷贝纯文本文件使用字符流,拷贝其他(图片，音频，视频)使用在字节流
 */
public class Stream类字节流 {

    public static <fileOutputStream> void main(String[] args) throws IOException {
        /**
         * 字节形式的
         */
//        //1.创建字符输入流，关联数据源文件
//        FileInputStream fileInputStream = new FileInputStream("lib/a/java.png");
//        //2..创建字符输出流，关联目的地文件
//        FileOutputStream fileOutputStream = new FileOutputStream("lib/a/b.png");
//        //3.读数据
//        int len;
//        while ((len=fileInputStream.read())!=-1){
//        //4.写数据
//            fileOutputStream.write(len);
//        }
//        //5.释放资源
//        fileInputStream.close();
//        fileOutputStream.close();
        /**
         * 数组形式的
         */
//        FileInputStream fileInputStream1 = new FileInputStream("lib/a/java.png");
//        FileOutputStream fileOutputStream1 = new FileOutputStream("lib/a/c.png");
//        int len2;
//        byte[] chars=new byte[2048];
//        while ((len2=fileInputStream1.read(chars))!=-1){
//
//            fileOutputStream1.write(chars,0,len2);
//        }
//        fileInputStream1.close();
//        fileOutputStream1.close();
        /**
         * 字节缓冲流用法：
         */
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream("lib/a/java.png"));
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("lib/a/d.png"));
        int lengz;
        while ((lengz=inputStream.read())!=-1){
            outputStream.write(lengz);
        }
        inputStream.close();
        outputStream.close();
    }
}
