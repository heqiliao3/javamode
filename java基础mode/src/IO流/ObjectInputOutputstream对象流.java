package IO流;

import IO流.entity.Person;
import org.junit.Test;


import java.io.*;

/**
 * 对象流分为两种：
 *   objectInputStream
 *   ObjectOutputStream
 */
public class ObjectInputOutputstream对象流 {
    /**
     * 序列号的过程：将内存的java对象保存到磁盘中或通过网络传输出去
     * 使用ObjectOutputStream
     */

    @Test
    public  void TextObjectOutputStream() throws IOException {

        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("object.txt"));
        stream.writeObject(new String("哈哈哈"));
        stream.writeObject(new Person("张三",15));
        stream.flush();//刷新操作
        stream.close();
    }
    /**
     * 反序列化过程：
     */
    @Test
    public  void TextobjectInputStream() throws IOException, ClassNotFoundException {
        ObjectInputStream Input=null;
        Input = new ObjectInputStream(new FileInputStream("object.txt"));
        Object object = Input.readObject();
        String name= (String) object;
        Person person= (Person) Input.readObject();
        System.out.println(name);
        System.out.println(person);
        Input.close();

    }
}
