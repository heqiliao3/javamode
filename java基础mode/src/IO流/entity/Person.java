package IO流.entity;

import java.io.Serializable;

/**
 * person 需要满足如下的要求，方可序列化：
 *   1.接口必须实现：Serializable;
 *   2.当前类提供一个全局常量：serialVersionUID；
 *   3.除了当前接口实现 Serializable接口以外，还必须保证其内部所有属性也必须是可序列化的;(默认情况下，基本数据类型都是可序列化的)
 *   4.不能序列化statis 和transient修饰的成员变量;
 */

public class Person implements Serializable {

    private static final long serialVersionUID = 22710L;
    private String name;
    private   int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
