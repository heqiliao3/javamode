package IO流;


import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 需求：从1.txt文件的数据拷贝到2.txt文件 一次一个字符
 */
public class 练习file类和Reader类 {
    public static void main(String[] args) throws IOException {
//        //创建字符输入流对象
//        FileReader fileReader=new FileReader("lib/a/1.txt");
//        //创建字符输出流对象  如果没有目的地文件，会创建
//        FileWriter fileWriter=new FileWriter("lib/a/3.txt");
//        int ch;
//        //读数据
//        while ((ch= fileReader.read())!=-1){
//            //写数据
//            fileWriter.write(ch);
//        }
//      //释放资源
//        fileReader.close();
//        fileWriter.close();
        /**
         * 需求：从1.txt文件的数据拷贝到 2.txt文件 一次一个[]数组
         */
        FileReader reader = new FileReader("lib/a/1.txt");
        FileWriter writer = new FileWriter("lib/a/2.txt");
         int ch2;
          char[] chars=new char[1024];
          while ((ch2=reader.read(chars))!=-1){
              writer.write(chars,0,ch2);
          }
          reader.close();
          writer.close();
    }


}
