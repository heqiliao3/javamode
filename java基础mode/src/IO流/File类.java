package IO流;

import java.io.File;
import java.io.IOException;

/**
 *  File类：
 *     概述：
 *       就是用来操作文件夹路径的;
 *  构造方法：
 *     File(String pathname) 根据给定的字符串路径创建其对应file对象;
 *     File(String parent, String child) 根据给定的字符串形式的父目录和子文件夹名创建file对象
 *     File(String parent, String child) 根据给定的父目录对象和子文件创建file对象;
 *  成员方法：
 *     创建功能：如果不存在就创建，如果存在就不创建
 *        createNewFile():创建文件
 *        mkdir():        创建单级目录
 *        mkdirs():       创建目录
 *     判断功能：
 *        isDirectory():判断file对象是否为目录;
 *        isFile():判断file对象是否为文件;
 *        exists():判断file对象是否存在
 *     获取功能：
 *        getAbsoIutePath(): 获取绝对路径;
 *        getPath(): 获取文件的相对路径
 *        getName(): 获取文件名
 *        list(): 获取指定目录下所有文件(夹)名称数组
 *        listFile():获取指定目录下所有文件（夹）File数组；
 *
 *
 */
public class File类 {
    public static void main(String[] args) throws IOException {
        // D:\开发工具包\开发工具包\测试用.txt 文件
        //构造方法一
        //File file =new File("D:\\开发工具包\\开发工具包\\测试用.txt");
        File file =new File("D:/开发工具包/开发工具包/测试用.txt");
        System.out.println("file:"+file);
        //构造方法二
        File file2 =new File("D:\\开发工具包\\开发工具包","测试用.txt");
        System.out.println("file2:"+file2);
        //构造方法三
        File file3 =new File("D:\\开发工具包\\开发工具包");
        File file4 =new File(file3,"测试用.txt");
        System.out.println("file4:"+file4);
        // createNewFile():创建文件
        File file5 =new File("D:/开发工具包/开发工具包/测试2.txt");
        boolean newFile = file5.createNewFile();
        System.out.println("newFile:"+""+newFile);
        //mkdir():创建单级目录
        File file6 =new File("D:/开发工具包/开发工具包/a");
        boolean newFile2 = file6.mkdir();
        System.out.println("newFile2:"+""+newFile2);
        //mkdirs():创建目录
        File file7 =new File("D:/开发工具包/开发工具包/a/b");
        boolean newFile3 = file7.mkdir();
        System.out.println("newFile3:"+""+newFile3);
        /**
         *  判断功能：
         *  *        isDirectory():判断file对象是否为目录;
         *  *        isFile():判断file对象是否为文件;
         *  *        exists():判断file对象是否存在
         */
        File file8 =new File("D:/开发工具包/开发工具包/a/b");
        System.out.println("判断file对象是否为目录:"+file8.isDirectory());
        System.out.println("判断file对象是否为文件:"+file8.isFile());
        System.out.println("判断file对象是否存在:"+file8.exists());
        /**
         * 获取功能：
         *  *        getAbsoIutePath(): 获取绝对路径;
         *  *        getPath(): 获取文件的相对路径
         *  *        getName(): 获取文件名
         *  *        list(): 获取指定目录下所有文件(夹)名称数组
         *  *        listFile():获取指定目录下所有文件（夹）File数组；
         */
        File file1 = new File("1.txt");
        //getAbsoIutePath(): 获取绝对路径;
        System.out.println("绝对路径:"+file1.getAbsolutePath());
        //getPath(): 获取文件的相对路径
        System.out.println("相对路径:"+file1.getPath());
        // list(): 获取指定目录下所有文件(夹)名称数组
        File file9 = new File("lib");
        String[] list = file9.list();
        for (String name:list)
        {
            System.out.println("name:"+name);
        }
        System.out.println("获取指定目录下所有文件（夹）File数组；");
        //listFile():获取指定目录下所有文件（夹）File数组；
        File[] files = file9.listFiles();
        for (File name:files)
        {
            System.out.println("name:"+name);
        }
    }
}
