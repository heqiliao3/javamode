package IO流;



import org.junit.Test;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * 1.RandomAccessFile 直接继承了object类，实现DataInput和DataOutput接口;
 * 2.RandomAccessFile既可以做为一个输入流，又可以作为一个输出流;
 */
public class RandomAccessFileTest随机读写流 {

    @Test
   public void text1() throws IOException {
        RandomAccessFile file = new RandomAccessFile("lib/a/java.png","r");
        RandomAccessFile file2 = new RandomAccessFile("lib/a/java1.png","rw");
        byte[] bytes=new byte[1024];
        int len;
        while ((len=file.read(bytes))!=-1){
             file2.write(bytes,0,len);
        }
        file.close();
        file2.close();
    }
}
