package IO流;

import java.awt.print.Book;
import java.io.*;
import java.util.Scanner;

/**
 * 需求：模拟用户上传头像的功能
 */
public class 上传头像练习 {
    public static void main(String[] args) throws IOException {
        /**
         * 1.定义一个方法，用来回去上传的用户头像的路径  getpath()
         * 2.定义一个方法，用来判断要上传的用户头像，在lib文件夹中是否存在
         * 3.如果存在，就失败，如果不存在就成功；
         *
         */
       File file=getpath();
        System.out.println(file);
        System.out.println(file.getName());
        //file.getName() 就会只传路径中的图片名字和格式
        boolean exists = isExists(file.getName());
        if(exists){
            System.out.println("该用户头像已存在");
        }else {
             sctp(file);
             System.out.println("上传成功");

        }
    }
    //用来回去上传的用户头像的路径  getpath()
    public static File getpath(){
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("请录入你要上传图片的路径：");
            String path=scanner.nextLine();

            if(!path.endsWith(".jsp") && !path.endsWith(".png") && !path.endsWith(".bmp")){
                System.out.println("您录入的不是图片,请重新录入!");
                continue;
            }
            File file = new File(path);
            if(file.exists() && file.isFile()){
                return file;
            }else {
                System.out.println("您录入的路径不存在");
            }
        }


    }
    //用来判断要上传的用户头像，在lib文件夹中是否存在
    public static boolean isExists(String path){
        File file = new File("lib/a");
        String[] names=file.list();
        for (String name:names){
           if(name.equals(path)){
               return true;
           }
        }
        return false;
    }
    //上传文件
    public static void sctp(File path) throws IOException {
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(path));
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("lib"+"/a/"+path.getName()));
        int leng;
          while ((leng=inputStream.read())!=-1){
              outputStream.write(leng);
          }
        inputStream.close();
          outputStream.close();
    }
}
