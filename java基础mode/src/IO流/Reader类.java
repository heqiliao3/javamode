package IO流;

import java.io.*;

/**
 * 字符流读数据：
 *    Reader类中的方法：
 *      int read(): 读一个字符，返回字符对应的ascii码值，读不到返回-1;
 *    FileReader类的构造方法：
 *      public FileReader(String pathname): 根据传入的字符串形式的路径，获取字符输入流对象;
 *  字符流写入数据：
 *     Writer类中的方法：
 *         void write(int ch);      一次写一个字符
 *         void write(char[] chs,int index,int len) 一次写一个指定的字符数组
 *         void write(String str);          一次写一个字符串
 *    FlieWriter类的构造方法：
 *    public FileWriter(String pathnam); 根据传入的字符串形式的路径，获取文件
 *  字符缓冲流用法：
 *     分类：
 *       BufferedReader:字符缓冲输入流
 *         构造方法：
 *           public BufferedReader(Reader reader)
 *         成员方法：
 *           public String readline();一次读取一行数据并返回读取到的内容，读不到返回null
 *       BufferedReader:字符缓冲输出流
 *          构造方法：
 *            public BufferedWriter(Writer writer)
 *          成员方法：
 *            public void newLine(); 根据当前操作系统给出对应的换行符；
 *      特点：
 *          字符缓冲流自带有缓冲区，大小为8192个字符，也就是16kb;
 *
 *
 * @author 白小布
 */
public class Reader类 {
    public static void main(String[] args) throws IOException {
//        /**
//         * 字符流读数据：
//         */
//        //1.创建字符输入流对象
//        FileReader reader = new FileReader("lib/a/1.txt");
//        //2.读取数据
//        int ch;
//        while ((ch=reader.read())!= -1){
//
//            System.out.println(ch);
//        }
//        //3.释放资源
//        reader.close();
        /**
         * 字符流写入数据：
         */
        //1.创建字符输出流对象
       // FileWriter writer = new FileWriter("lib/a/1.txt");
        //2.写数据 (第一种写法)
       //writer.write('好');
       //（第二种写法)
       // char[] chars={'黑','马','程','序','员'};
        //获取数组的全部
        //writer.write(chars);
        //获取数组的后三个
        //writer.write(chars,2,3);
        //第三种写法
        //writer.write("好好学习，天天向上");
        //3.释放资源
       // writer.close();
//        /**
//         * 字符缓冲流用法：
//         */
//        BufferedReader bufferedReader = new BufferedReader(new FileReader("lib/a/1.txt"));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("lib/a/2.txt"));
//        int ch2;
//        while ((ch2=bufferedReader.read())!=-1){
//            bufferedWriter.write(ch2);
//
//        }
//        bufferedReader.close();
//        bufferedWriter.close();
        /**
         * 字符缓冲流用法：
         */
        BufferedReader bufferedReader = new BufferedReader(new FileReader("lib/a/1.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("lib/a/2.txt"));
        String ch3;
        while ((ch3=bufferedReader.readLine())!=null){
            bufferedWriter.write(ch3);
            bufferedWriter.newLine();

        }
        bufferedReader.close();
        bufferedWriter.close();

    }
}
